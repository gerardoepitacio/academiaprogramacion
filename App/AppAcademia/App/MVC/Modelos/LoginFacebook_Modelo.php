<?php

	class LoginFacebook_Modelo extends AppSQLConsultas {

		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultarUsuario($IdFacebook = false)
		 *
		 * Consulta de un usuario a travez de su facebook ID
		 * @param bool $IdFacebook
		 * @return array
		 */
		public function ConsultarUsuario($IdFacebook = false) {
			if($IdFacebook == true AND $IdFacebook != '') {
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_informacion_usuarios', false, false, APP), self::ListarColumnas('tbl_sistema_usuarios', array('Password', 'Status'), false, APP)));
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.IdFacebook = '$IdFacebook'");
				$Consulta->Condicion("tbl_sistema_usuarios.AccessToken != ''");
				$Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarUsuarioEmail($Email = false)
		 *
		 * Consultar de un usuario a travez de su email
		 * @param bool $Email
		 * @return array
		 */
		public function ConsultarUsuarioEmail($Email = false) {
			if($Email == true AND $Email != '') {
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_informacion_usuarios', false, false, APP), self::ListarColumnas('tbl_sistema_usuarios', array('Password', 'Status'), false, APP)));
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
				$Consulta->Condicion("tbl_informacion_usuarios.Correo = '$Email'");
				$Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarPermisos($Permisos = false)
		 *
		 * Genera la consulta de los datos correspondientes
		 * @param $Permiso: Identificador del permiso
		 * @return array
		 */
		public function ConsultarPermisos($Permisos = false) {
			if($Permisos == true AND is_numeric($Permisos) == true) {
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios_perfil');
				$Consulta->Columnas(self::ListarColumnas('tbl_sistema_usuarios_perfil', array('IdPerfil', 'Status'), false, APP));
				$Consulta->Condicion("IdPerfil = '$Permisos'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * GuardarUsuario($Datos = false)
		 *
		 * Genera un nuevo usuario
		 * @param bool $Datos
		 * @return mixed
		 */
		public function GuardarUsuario($Datos = false){
			if($Datos == true AND is_array($Datos)){
				try{
					$this->Conexion->insert('tbl_sistema_usuarios', $Datos);
					return $this->Conexion->lastInsertId();
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

		/**
		 * Metodo Publico
		 * GuardarInfoUsuario($Datos = false)
		 *
		 * Guarda la informacion del usuario
		 * @param bool $Datos
		 * @return mixed
		 */
		public function GuardarInfoUsuario($Datos = false){
			if($Datos == true AND is_array($Datos)){
				try{
					$this->Conexion->insert('tbl_informacion_usuarios', $Datos);
					return $this->Conexion->lastInsertId();
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarDatosUsuario($Datos = false, $IdUsuario = false)
		 *
		 * Actualiza los datos del usuario
		 * @param bool $Datos
		 * @param bool $IdUsuario
		 */
		public function ActualizarDatosUsuario($Datos = false, $IdUsuario = false){
			if($Datos == true AND is_array($Datos) AND $IdUsuario == true AND $IdUsuario != ''){
				$this->Conexion->update('tbl_sistema_usuarios', $Datos, array('IdUsuario'=>$IdUsuario));
			}
		}

	}