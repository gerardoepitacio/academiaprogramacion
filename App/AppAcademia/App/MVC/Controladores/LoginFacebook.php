<?php

class LoginFacebook extends Controlador{

	/**
	 * LoginFacebook::__Construct()
	 *
	 * Genera la validacion de la sesion a travez de facebook
	 */
	function __Construct(){
		parent::__Construct();
		NeuralSesiones::Inicializar(APP);
		if (isset($_SESSION, $_SESSION['UOAUTH_APP']) == true) {
			header('Location:' . NeuralRutasApp::RutaUrlAppModulo('Central'));
			exit();
		}
	}

	public function Index(){
	}

	/**
	 * Metodo Publico
	 * FacebookCallback()
	 *
	 * Recibe el callback de llamado a facebook
	 */
	public function FacebookCallback(){
		if (isset($_GET) == true AND is_array($_GET) == true) {
			if (isset($_GET['error']) == true AND $_GET['error'] == 'access_denied' AND $_GET['error_code'] == 200) {
				header('Location:' . NeuralRutasApp::RutaUrlApp('Index', 'Login'));
				exit();
			} else {
				$fb = new AppFacebook();
				$AccessToken = $fb->ObtenerAccessToken();
				if (isset($AccessToken) == true and is_array($AccessToken) == false) {
					$AccessToken = $fb->ExtenderAccessToken($AccessToken);
					$Token = $AccessToken->getValue();
					$fb->EstablecerDefaultAccessToken($Token);
					$Informacion = $fb->ObtenerInformacionUsuario();
					if ($Informacion == true AND is_array($Informacion) == true AND isset($Informacion['id']) == true) {
						self::ValidarUsuario($Informacion, $AccessToken);
					} else {
						header('Location:' . NeuralRutasApp::RutaUrlApp('Index', 'Login'));
						exit();
					}
				} else {
					header('Location:' . NeuralRutasApp::RutaUrlApp('Index', 'Login'));
					exit();
				}
			}
		}
	}

	/**
	 * Metodo Publico
	 * ValidarUsuario($Informacion = false, $AccessToken = false)
	 *
	 * Valida la sesion del usuario, si es un usuario registrado, es redireccionado a su modulo, si el usuario no se
	 * encuentra registrado, se inicia el registro
	 * @param bool $Informacion
	 * @param bool $AccessToken
	 */
	private function ValidarUsuario($Informacion = false, $AccessToken = false){
		if ($Informacion == true AND is_array($Informacion) AND $AccessToken == true AND $AccessToken != '') {
			$Consulta = $this->Modelo->ConsultarUsuario($Informacion['id']);
			if ($Consulta['Cantidad'] == 1):
				$this->AutenticacionConsultaPermisos($Consulta);
			else:
				if(isset($Informacion['email']) == true AND $Informacion['email'] != ''){
					$Consulta = $this->Modelo->ConsultarUsuarioEmail($Informacion['email']);
					if ($Consulta['Cantidad'] == 1):
						self::ActualizarDatosUsuario($Consulta, $Informacion, $AccessToken);
						$this->AutenticacionConsultaPermisos($Consulta);
					else:
						$this->CrearUsuario($Informacion, $AccessToken);
					endif;
				}else{
					$this->CrearUsuario($Informacion, $AccessToken);
				}
			endif;
		}
	}

	/**
	 * LoginFacebook::AutenticacionConsultaPermisos()
	 *
	 * Genera la consulta de los permisos correspondientes
	 * @return ok
	 * @param bool $Consulta : Consulta Usuario
	 */
	private function AutenticacionConsultaPermisos($Consulta = false){
		$ConsultaPermisos = $this->Modelo->ConsultarPermisos($Consulta[0]['IdPerfil']);
		if ($ConsultaPermisos['Cantidad'] == 1):
			AppSession::Registrar($Consulta[0], $ConsultaPermisos[0]);
			header("Location: " . NeuralRutasApp::RutaUrlAppModulo('Central'));
			exit();
		else:
			header("Location: " . NeuralRutasApp::RutaUrlApp('LogOut'));
			exit();
		endif;
	}

	/**
	 * Metodo Publico
	 * CrearUsuario($Informacion = false, $AccessToken = false)
	 *
	 * Crea un nuevo usuario a travez de facebook
	 * @param bool $Informacion: Informacion de facebook
	 * @param bool $AccessToken: Token de acceso
	 */
	private function CrearUsuario($Informacion = false, $AccessToken = false){
		if($Informacion == true AND is_array($Informacion) AND $AccessToken == true AND $AccessToken != ''){
			$Usuario = array(
				'IdPerfil'=>'3',
				'Status'=>'ACTIVO',
				'IdFacebook'=>$Informacion['id'],
				'AccessToken'=>$AccessToken->getValue()
			);
			$IdUsuario = $this->Modelo->GuardarUsuario($Usuario);
			if($IdUsuario == true and $IdUsuario != ''){
				$InformacionUsuario = array(
					'IdUsuario'=>$IdUsuario,
					'Nombres'=>$Informacion['name'],
					'Cargo'=>'Asistente',
					'Correo'=>(isset($Informacion['email']) == true AND $Informacion['email'] != '') ? $Informacion['email'] : '',
					'Status'=>'ACTIVO',
				);
				$this->Modelo->GuardarInfoUsuario($InformacionUsuario);
				$Consulta = $this->Modelo->ConsultarUsuario($Informacion['id']);
				unset($Usuario, $IdUsuario, $InformacionUsuario);
				$this->AutenticacionConsultaPermisos($Consulta);
			}
		}
	}

	/**
	 * Metodo Publico
	 * ActualizarDatosUsuario($Consulta = false, $Informacion = false, $AccessToken = false)
	 *
	 * Actualiza los datos del usuario si ya existe.
	 * @param bool $Consulta: Consulta Usuario
	 * @param bool $Informacion: Informacion de facebook
	 * @param bool $AccessToken: Token de acceso
	 */
	private function ActualizarDatosUsuario($Consulta = false, $Informacion = false, $AccessToken = false){
		if($Consulta == true AND $Informacion == true AND $AccessToken == true){
			$Datos = array(
				'IdFacebook'=>$Informacion['id'],
				'AccessToken'=>$AccessToken->getValue()
			);
			$this->Modelo->ActualizarDatosUsuario($Datos, $Consulta[0]['IdUsuario']);
			unset($Datos);
		}
	}

}