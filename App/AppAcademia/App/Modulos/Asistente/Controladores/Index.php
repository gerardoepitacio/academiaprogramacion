<?php

	class Index extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Redireccion a los talleres disponibles.
		 */
		public function Index() {
			header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Asistente', 'Talleres'));
			exit();
		}
	}