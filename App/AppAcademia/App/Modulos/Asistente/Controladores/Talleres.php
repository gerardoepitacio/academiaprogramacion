<?php

	class Talleres extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 *
		 * @throws NeuralException
		 */
		public function Index() {
			$Talleres = $this->Modelo->ConsultarTalleresActivos($this->Informacion['Informacion']['IdInformacion']);
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Talleres', $Talleres);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Filtro('Cifrado', function ($Parametro) {
				return NeuralCriptografia::Codificar($Parametro, APP);
			});
			$Plantilla->Filtro('Ascii_Hex', function($Parametro){
				return AppConversores::ASCII_HEX($Parametro);
			});
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Talleres', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * MisTalleres()
		 *
		 * Lista los talleres del asistente
		 * @throws NeuralException
		 */
		public function MisTalleres(){
			$Talleres = $this->Modelo->ConsultarTalleresAsistente($this->Informacion['Informacion']['IdInformacion']);
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Talleres', $Talleres);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Filtro('Cifrado', function ($Parametro) {
				return NeuralCriptografia::Codificar($Parametro, APP);
			});
			$Plantilla->Filtro('Ascii_Hex', function($Parametro){
				return AppConversores::ASCII_HEX($Parametro);
			});
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Talleres', 'Index_MisTalleres.html')));
			unset($Talleres, $MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * VerTaller($IdTaller = false)
		 *
		 * Muestra el detalle del taller.
		 * @param bool $IdTaller
		 * @throws NeuralException
		 */
		public function VerTaller($IdTaller = false) {
			if($IdTaller == true AND $IdTaller != ''){
				set_error_handler(function() {
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Talleres', 'Index_ErrorContenido.html')));
					unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
					exit();
				});
				$IdTaller = NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($IdTaller), APP);
				restore_error_handler();
				$Taller = $this->Modelo->ConsultarTaller(array('tbl_talleres.IdTaller'=>$IdTaller), $this->Informacion['Informacion']['IdInformacion']);
				$Instructores = $this->Modelo->ConsultarInstructores(array('tbl_talleres.IdTaller'=>$IdTaller));
				$Temario = $this->Modelo->ConsultarTemario(array('tbl_unidades_taller.IdTaller'=>$IdTaller));
				$HorarioTaller = $this->Modelo->ConsultarDiasHorarios($IdTaller);
				$Temario = $this->AgruparTemasUnidad($Temario);
				$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
				$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
				$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
				$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
				$Plantilla->Parametro('Menu', $MenuSeleccion);
				$Plantilla->Parametro('Usuario', $Usuario);
				$Plantilla->Parametro('Taller', $Taller[0]);
				$Plantilla->Parametro('Temario', $Temario);
				$Plantilla->Parametro('Instructores', $Instructores);
				$Plantilla->Parametro('HorarioTaller', $HorarioTaller);
				$Plantilla->Filtro('Cifrado', function ($Parametro) {
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Filtro('Ascii_Hex', function($Parametro){
					return AppConversores::ASCII_HEX($Parametro);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Index.html')));
				unset($Taller, $Temario, $MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
				exit();
			}else{
				$Plantilla = new NeuralPlantillasTwig(APP);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Talleres', 'Index_ErrorContenido.html')));
				unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * InscribirUsuario()
		 *
		 * Inscribe a un usuario al taller
		 */
		public function InscribirUsuario(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if($_POST['IdTaller'] == true AND $_POST['IdTaller'] != ''){
					$IdTaller = NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['IdTaller']), APP);
					$Datos = array('IdTaller'=>$IdTaller, 'IdInformacionAsistente'=> $this->Informacion['Informacion']['IdInformacion']);
					$this->Modelo->AsignarTallerUsuario($Datos);
					unset($IdTaller, $Datos);
				}
			}
		}

		/**
		 * Metodo Publico
		 * AgruparTemasUnidad($Temario = false)
		 *
		 * Agrupa los temas con su respectiva unidad
		 * @param bool $Temario
		 * @return array
		 */
		private function AgruparTemasUnidad($Temario = false){
			if($Temario == true){
				$DatosTemario = [];
				$Unidades = array_unique(array_column($Temario, "Unidad"));
				foreach ($Unidades AS $key => $Unidad){
					$DatosTemario[$key]["Unidad"] = $Unidad;
					foreach ($Temario AS $Valor){
						if($Valor['Unidad'] == $Unidad){
							$DatosTemario[$key]["Temas"][] = $Valor['NombreTema'];
						}
					}
				}
				return $DatosTemario;
			}

		}

	}