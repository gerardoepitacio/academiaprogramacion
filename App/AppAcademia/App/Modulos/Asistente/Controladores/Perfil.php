<?php

	class Perfil extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Principal de talleres
		 * @throws NeuralException
		 */
		public function Index() {
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * frmEditar()
		 *
		 * Vista para editar el perfil.
		 * @throws NeuralException
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ObtenerInformacionUsuario(array('tbl_informacion_usuarios.IdInformacion'=>$this->Informacion['Informacion']['IdInformacion']));
				$CantidadInscritos = "0";
				$CantidadTerminados = "0";
				$CantidadCreditos = "0";
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Nombres','*Campo requerido');
				$Validacion->Requerido('Correo', '* Campo requerido');
				$Validacion->Requerido('Usuario', '* Campo requerido');
				$Validacion->CantMaxCaracteres('Nombres',255,'*Máximo 255 caracteres');
				$Validacion->CantMaxCaracteres('ApellidoPaterno',255, '* Máximo 255 Caracteres');
				$Validacion->CantMaxCaracteres('ApellidoMaterno',255, '* Máximo 255 Caracteres');
				$Validacion->CantMaxCaracteres('Correo',255,'* Máximo 255 Caracteres');
				$Validacion->Email('Correo', '* Correo no válido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* Las contraseñas no coinciden.');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('CantidadInscritos', $CantidadInscritos);
				$Plantilla->Parametro('CantidadTerminado', $CantidadTerminados);
				$Plantilla->Parametro('CantidadCreditos', $CantidadCreditos);
				$Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarPerfil'));
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Editar', 'EditarPerfil.html')));
				unset($Consulta, $CantidadInscritos, $CantidadTerminados, $CantidadCreditos, $Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * Actualizar()
		 *
		 * Actualiza los datos de perfil del usuario
		 * @throws NeuralException
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ) {
					$DatosPost=AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
					$DatosPost = AppPost::FormatoEspacio($DatosPost);
					unset($_POST,$DatosPost['Key']);
					if(isset($DatosPost['Password']) AND $DatosPost['Password'] != '' AND $DatosPost['Password'] == $DatosPost['RepitePassword']){
						$this->Modelo->ActualizarDatosUsuario(array('Usuario'=>$DatosPost['Usuario'], 'Password'=>hash('sha256',$DatosPost['Password'])), array('IdUsuario'=>$this->Informacion['Informacion']['IdUsuario']));
					}else{
						$this->Modelo->ActualizarDatosUsuario(array('Usuario'=>$DatosPost['Usuario']), array('IdUsuario'=>$this->Informacion['Informacion']['IdUsuario']));
					}
					unset($DatosPost['Password'], $DatosPost['RepitePassword'], $DatosPost['Usuario']);
					$this->Modelo->ActualizarInformacionUsuario($DatosPost, array('IdInformacion'=>$this->Informacion['Informacion']['IdInformacion']));
					self::frmEditar();
					exit();
				}else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Error', 'ErrorElementosRequeridos.html')));
					unset($Plantilla);
					exit();
				}
			}else{
				$Plantilla = new NeuralPlantillasTwig(APP);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Perfil', 'Error', 'ErrorElementosRequeridos.html')));
				unset($Plantilla);
				exit();
			}
		}

	}