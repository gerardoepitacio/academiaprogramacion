.<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Perfil_Modelo extends AppSQLConsultas {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		public function ObtenerInformacionUsuario($Condiciones = false) {
			if($Condiciones == true and is_array($Condiciones)){
				$Campos = implode(',', self::ListarColumnas('tbl_informacion_usuarios', false, false, APP));
				$Campos .= ', tbl_sistema_usuarios.Usuario';
				$SQL = "SELECT $Campos FROM tbl_informacion_usuarios".
					" INNER JOIN tbl_sistema_usuarios ON tbl_informacion_usuarios.IdUsuario = tbl_sistema_usuarios.IdUsuario".
					" WHERE tbl_sistema_usuarios.Status = 'ACTIVO'";
				if($Condiciones == true AND is_array($Condiciones))
					$SQL.= ' AND '.self::ObtenerCondicionesAND($Condiciones);
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarDatosUsuario($Datos = false, $Condiciones = false)
		 *
		 * Actualiza la informacion de sistema del usuario.
		 * @param bool $Datos
		 * @param bool $Condiciones
		 * @return mixed
		 */
		public function ActualizarDatosUsuario($Datos = false, $Condiciones = false){
			if($Datos == true AND is_array($Datos) == true AND $Condiciones == true AND is_array($Condiciones)){
				try{
					return $this->Conexion->update('tbl_sistema_usuarios', $Datos, $Condiciones);
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarInformacionUsuario($Datos = false, $Condiciones = false)
		 *
		 * Actualiza la informacion del usuario.
		 * @param bool $Datos
		 * @param bool $Condiciones
		 * @return mixed
		 */
		public function ActualizarInformacionUsuario($Datos = false, $Condiciones = false){
			if($Datos == true AND is_array($Datos) == true AND $Condiciones == true AND is_array($Condiciones)){
				try{
					return $this->Conexion->update('tbl_informacion_usuarios', $Datos, $Condiciones);
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

	}