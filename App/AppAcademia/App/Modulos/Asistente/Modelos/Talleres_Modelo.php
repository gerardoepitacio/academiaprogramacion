<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Talleres_Modelo extends AppSQLConsultas {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultarTalleresActivos($IdAsistente = false)
		 *
		 * Devuelve los talleres activos
		 * @param bool $IdAsistente
		 * @return mixed
		 */
		public function ConsultarTalleresActivos($IdAsistente = false) {
			if($IdAsistente == true AND $IdAsistente != ''){
				$Campos = implode(',', self::ListarColumnas('tbl_talleres', false, false, APP));
				$Campos.= ', '.implode(',', self::ListarColumnas('tbl_periodos', false, array('Nombre'=>'Periodo'), APP));
				$Campos.= ', tbl_talleres_asistentes.IdTallerAsistente';
				$SQL = "SELECT $Campos FROM tbl_talleres".
					" INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo".
					" LEFT JOIN tbl_talleres_asistentes ON tbl_talleres.IdTaller = tbl_talleres_asistentes.IdTaller AND tbl_talleres_asistentes.IdInformacionAsistente = '$IdAsistente'".
					" WHERE tbl_talleres.Status = 'ACTIVO' AND tbl_periodos.Status = 'ACTIVO'";
				$SQL .= " GROUP BY tbl_talleres.IdTaller ORDER BY tbl_talleres.FechaHoraCreacion DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarTalleresAsistente($IdAsistente = false)
		 * 
		 * Consulta los talleres del asistente
		 * @param bool $IdAsistente
		 * @return mixed
		 */
		public function ConsultarTalleresAsistente($IdAsistente = false) {
			if($IdAsistente == true AND $IdAsistente != ''){
				$Campos = implode(',', self::ListarColumnas('tbl_talleres', false, false, APP));
				$Campos.= ', '.implode(',', self::ListarColumnas('tbl_periodos', false, array('Nombre'=>'Periodo'), APP));
				$SQL = "SELECT $Campos FROM tbl_talleres".
					" INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo".
					" INNER JOIN tbl_talleres_asistentes ON tbl_talleres.IdTaller = tbl_talleres_asistentes.IdTaller"
					." WHERE tbl_talleres_asistentes.IdInformacionAsistente = '$IdAsistente' AND tbl_talleres.Status = 'ACTIVO' AND tbl_periodos.Status = 'ACTIVO'";
				$SQL .= " ORDER BY tbl_talleres.FechaHoraCreacion DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarTaller($Condiciones = false, $IdAsistente = true)
		 *
		 * Devuelve los detalles del taller.
		 * @param bool $Condiciones
		 * @param bool $IdAsistente
		 * @return mixed
		 */
		public function ConsultarTaller($Condiciones = false, $IdAsistente = true){
			if($Condiciones == true AND is_array($Condiciones) == true AND $IdAsistente == true AND $IdAsistente != ''){
				$Campos = implode(',', self::ListarColumnas('tbl_talleres', false, false, APP));
				$Campos.= ', '.implode(',', self::ListarColumnas('tbl_periodos', false, array('Nombre'=>'Periodo'), APP));
				$Campos.= ', tbl_talleres_asistentes.IdTallerAsistente ';
				$Campos.=', tbl_informacion_usuarios.IdInformacion, 
				tbl_informacion_usuarios.Nombres AS NombreInstructor, 
				tbl_informacion_usuarios.ApellidoPaterno, 
				tbl_informacion_usuarios.Cargo AS CargoInstructor,
				tbl_informacion_usuarios.NombreImagen AS ImagenInstructor,
				tbl_informacion_usuarios.Descripcion AS DescripcionInstructor';
				$SQL = "SELECT $Campos FROM tbl_talleres".
					" INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo".
					" INNER JOIN tbl_instructores_talleres ON tbl_talleres.IdTaller = tbl_instructores_talleres.IdTaller".
					" INNER JOIN tbl_informacion_usuarios ON tbl_instructores_talleres.IdInformacionInstructor = tbl_informacion_usuarios.IdInformacion ".
					" LEFT JOIN tbl_talleres_asistentes ON tbl_talleres.IdTaller = tbl_talleres_asistentes.IdTaller AND tbl_talleres_asistentes.IdInformacionAsistente = '$IdAsistente'".
					" WHERE tbl_talleres.Status = 'ACTIVO' AND tbl_periodos.Status = 'ACTIVO' AND tbl_instructores_talleres.InstructorPrincipal = 'ACTIVO'";
				if($Condiciones == true AND is_array($Condiciones))
					$SQL.= " AND ".self::ObtenerCondicionesAND($Condiciones);
				$SQL .= " ORDER BY tbl_talleres.FechaHoraCreacion DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarInstructores($Condiciones = false)
		 *
		 * Devuelve los instructores del taller.
		 * @param bool $Condiciones
		 * @return mixed
		 */
		public function ConsultarInstructores($Condiciones = false){
			if($Condiciones == true AND is_array($Condiciones) == true){
				$Campos = " CONCAT(Nombres, ' ', ApellidoPaterno) AS Instructor, tbl_informacion_usuarios.Descripcion AS DescripcionInstructor, NombreImagen, Cargo, tbl_instructores_talleres.InstructorPrincipal, UrlFacebook ";
				$SQL = "SELECT $Campos FROM tbl_talleres".
					" INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo".
					" INNER JOIN tbl_instructores_talleres ON tbl_talleres.IdTaller = tbl_instructores_talleres.IdTaller".
					" INNER JOIN tbl_informacion_usuarios ON tbl_instructores_talleres.IdInformacionInstructor = tbl_informacion_usuarios.IdInformacion".
					" WHERE tbl_talleres.Status = 'ACTIVO' AND tbl_periodos.Status = 'ACTIVO' AND tbl_informacion_usuarios.Status = 'ACTIVO' ";
				if($Condiciones == true AND is_array($Condiciones))
					$SQL.= " AND ".self::ObtenerCondicionesAND($Condiciones);
				$SQL .= " ORDER BY tbl_talleres.FechaHoraCreacion DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarDiasHorarios($IdTaller = false)
		 * 
		 * Consulta los dias y el horario correspondiente de un taller
		 * @param bool $IdTaller
		 * @return mixed
		 */
		public function ConsultarDiasHorarios($IdTaller = false){
			if($IdTaller == true AND $IdTaller != ''){
				$Campos="NombreDia, HoraInicio, HoraFin, tbl_taller_horarios.IdDia, tbl_taller_horarios.IdHorario";
				$SQL = "SELECT $Campos FROM tbl_taller_horarios".
					" INNER JOIN tbl_dias ON tbl_taller_horarios.IdDia = tbl_dias.IdDia".
					" INNER JOIN tbl_horarios ON tbl_taller_horarios.IdHorario = tbl_horarios.IdHorario";
				$SQL.= " WHERE tbl_taller_horarios.IdTaller = $IdTaller";
				$SQL.= " ORDER BY tbl_dias.IdDia";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarTemario($Condiciones = false)
		 *
		 * Consulta los temas del taller
		 * @param bool $Condiciones
		 * @return mixed
		 */
		public function ConsultarTemario($Condiciones = false){
			if($Condiciones == true AND is_array($Condiciones) == true){
				$Campos = "NombreUnidad As Unidad, NombreTema";
				$SQL = "SELECT $Campos FROM tbl_unidades_taller".
					" INNER JOIN tbl_temas_unidad ON tbl_unidades_taller.IdUnidad = tbl_temas_unidad.IdUnidad".
					" WHERE tbl_temas_unidad.Status = 'ACTIVO'";
				if($Condiciones == true AND is_array($Condiciones))
					$SQL.= " AND ".self::ObtenerCondicionesAND($Condiciones);
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * AsignarTallerUsuario($Datos = false)
		 *
		 * Asocia un registro con un taller.
		 * @param bool $Datos
		 */
		public function AsignarTallerUsuario($Datos = false){
			if($Datos == true AND is_array($Datos) == true){
				$this->Conexion->insert('tbl_talleres_asistentes', $Datos);
			}
		}

	}