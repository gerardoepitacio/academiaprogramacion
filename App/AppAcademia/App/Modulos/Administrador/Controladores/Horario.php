<?php

class Horario extends Controlador
{

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del controlador Horario
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos los Horarios
     */
    public function frmListado()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarHorario();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * frmAgregar()
     *
     * Funcion para el Formulario Horario
     * @throws NeuralException
     */
    public function frmAgregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('HoraInicio', '* Campo Requerido');
            $Validacion->Requerido('HoraFin', '* Campo Requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarHorario'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Agregar', 'frmAgregar.html')));
            unset($Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Horario
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->GuardaHorario($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Agregar', 'Exito.html')));
                unset($DatosPost, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * frmEditar()
     *
     * Formulario para editar Horario.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdHorario']) == true AND $_POST['IdHorario'] != '') {
                $IdHorario = NeuralCriptografia::DeCodificar($_POST['IdHorario'], APP);
                $Consulta = $this->Modelo->ConsultarHorario(array('IdHorario' => $IdHorario));
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('HoraInicio', '* Campo Requerido');
                $Validacion->Requerido('HoraFin', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarTaller'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Editar', 'frmEditar.html')));
                unset($IdHorario, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar Horario
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdHorario = NeuralCriptografia::DeCodificar($_POST['IdHorario'], APP);
                unset($_POST['IdHorario'],$_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->EditarDatos($DatosPost, $IdHorario);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Horario', 'Agregar', 'Exito.html')));
                unset($IdHorario, $DatosPost, $Plantilla);
                exit();
            }
        }
    }
}