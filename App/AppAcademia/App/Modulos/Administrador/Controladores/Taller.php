<?php

	class Taller extends Controlador
	{

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla Principal del controlador Taller
		 */
		public function Index(){
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico 
		 * ConsultaDiasHorario()
		 * 
		 * Lista el horario del taller
		 * @throws NeuralException
		 */
		public function ConsultaDiasHorario(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				$Consulta=$this->Modelo->ConsultarDiasHorarios(NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP));
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('HorarioTaller', $Consulta);
				if (isset($Consulta) == true AND is_array($Consulta) == true AND count($Consulta) > 0) {
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Modal', 'TablaHorarios.html')));
					unset($Consulta, $Plantilla);
					exit;
				}else{
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Modal', 'SinHorario.html')));
					unset($Consulta, $Plantilla);
					exit;
				}
			}
		}

		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Lista los talleres no eliminados
		 */
		public function frmListado(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				$Consulta = $this->Modelo->ConsultarTalleres();
				$Plantilla = new NeuralPlantillasTwig(APP);

				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function ($Parametro) {
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo publico
		 * frmAgregar()
		 *
		 * Formulario para agregar taller.
		 * @throws NeuralException
		 */
		public function frmAgregar(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				$Instructores = $this->Modelo->ConsultarInstructores();
				$Periodos = $this->Modelo->ConsultarPeriodos();
				$Dias = $this->Modelo->ConsultarDias();
				$Horarios = $this->Modelo->ConsultarHorario();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdPeriodo', '* Campo Requerido');
				$Validacion->Requerido('Nombre', '* Campo Requerido');
				$Validacion->Requerido('NombreIcono', '* Campo Requerido');
				$Validacion->Requerido('Descripcion', '* Campo Requerido');
				$Validacion->Requerido('Lugar', '* Campo Requerido');
				$Validacion->Requerido('IdInstructor', '* Campo Requerido');
				$Validacion->CantMinCaracteres('Descripcion',150, '* Minimo 150 caracteres');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Instructores', $Instructores);
				$Plantilla->Parametro('Periodos', $Periodos);
				$Plantilla->Parametro('Dias', $Dias);
				$Plantilla->Parametro('Horarios', $Horarios);
				$Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarTaller'));
				$Plantilla->Filtro('Cifrado', function ($Parametro) {
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Agregar', 'frmAgregar.html')));
				unset($Instructores, $Periodos, $Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * ObtenerAuxiliares()
		 *
		 * Metodo que devuelve el select para auxiliares.
		 * @throws NeuralException
		 */
		public function ObtenerAuxiliares(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if (isset($_POST) == true AND isset($_POST['IdInstructor'])) {
					$Instructores = $this->Modelo->ConsultarInstructores(NeuralCriptografia::DeCodificar($_POST['IdInstructor'], APP));
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Instructores', $Instructores);
					$Plantilla->Filtro('Cifrado', function ($Parametro) {
						return NeuralCriptografia::Codificar($Parametro, APP);
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Agregar', 'SelectAuxiliares.html')));
					unset($Instructores, $Plantilla);
					exit();
				}
			}
		}

		/**
		 * Metodo Publico
		 * Agregar()
		 *
		 * Funcion de agregar taller
		 * @throws NeuralException
		 */
		public function Agregar(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
					if (AppPost::DatosVaciosOmitidos($_POST, array('IdAuxiliares')) == false AND AppPost::DatosVaciosOmitidos($_POST, array('IdHorarios')) == false AND AppPost::DatosVaciosOmitidos($_POST, array('IdDias')) == false) {
						if (isset($_POST['IdAuxiliares']) == true)
							$IdAuxiliares = $_POST['IdAuxiliares'];

						if (isset($_POST['IdHorarios']) == true)
							$IdHorarios = $_POST['IdHorarios'];

						if (isset($_POST['IdDias']) == true)
							$IdDias = $_POST['IdDias'];

						$IdInstructor = NeuralCriptografia::DeCodificar($_POST['IdInstructor'], APP);
						unset($_POST['Key'], $_POST['IdAuxiliares'], $_POST['IdInstructor'], $_POST['IdHorarios'], $_POST['IdDias']);

						$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
						$DatosPost['IdPeriodo'] = NeuralCriptografia::DeCodificar($DatosPost['IdPeriodo']);
						$DatosPost['FechaHoraCreacion'] = AppFechas::ObtenerDatetimeActual();

						$IdTaller = $this->Modelo->GuardarDatos($DatosPost);
						if (isset($IdTaller) == true) {
							$DatosInstructor = array(
								'IdInformacionInstructor' => $IdInstructor,
								'IdTaller' => $IdTaller,
								'InstructorPrincipal' => 'ACTIVO');

							$this->Modelo->GuardarInstructor($DatosInstructor);
							if (isset($IdAuxiliares) == true AND is_array($IdAuxiliares) == true AND count($IdAuxiliares) > 0) {
								foreach ($IdAuxiliares as $IdAuxiliar) {
									$this->Modelo->GuardarInstructor(array(
										'IdInformacionInstructor' => NeuralCriptografia::DeCodificar($IdAuxiliar, APP),
										'IdTaller' => $IdTaller));
								}
							}

							if (isset($IdHorarios) == true AND is_array($IdHorarios) == true AND count($IdHorarios) > 0 AND isset($IdDias) == true AND is_array($IdDias) == true AND count($IdDias) > 0){
								$PosicionIdDia = current($IdDias);
								foreach ($IdHorarios as $IdHorario){
										$this->Modelo->GuardaTallerHorario(array(
											'IdHorario' => NeuralCriptografia::DeCodificar($IdHorario, APP),
											'IdDia' => NeuralCriptografia::DeCodificar($PosicionIdDia, APP),
											'IdTaller' => $IdTaller));
										$PosicionIdDia = next($IdDias);
								}
							}

							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Agregar', 'Exito.html')));
							unset($IdAuxiliares, $IdInstructor, $DatosPost, $IdTaller, $DatosInstructor, $IdAuxiliar, $IdHorarios, $IdHorario, $IdDias, $PosicionIdDia, $Plantilla);
							exit();
						} else {
							// -- Mensaje de error al insertar datos
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Error', 'ErrorInsertandoDatos.html')));
							unset($IdAuxiliares, $IdInstructor, $DatosPost, $IdTaller, $Plantilla);
							exit();
						}
					} else {
						// -- Mensaje elementos requeridos para la captura
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * frmEditar()
		 *
		 * Formulario para editar taller.
		 * @throws NeuralException
		 */
		public function frmEditar(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
					$IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
					$Consulta = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller));
					$Instructores = $this->Modelo->ConsultarInstructores();
					$Periodos = $this->Modelo->ConsultarPeriodos();
					$Auxiliares = $this->Modelo->ConsultarInstructores($Consulta[0]['IdInformacion']);
					$AuxiliaresSeleccionados = $this->Modelo->ConsultaAuxiliaresTaller(array('tbl_talleres.IdTaller' => $IdTaller));
					$HorarioTaller =  $this->Modelo->ConsultarDiasHorarios($IdTaller);
					$Dias = $this->Modelo->ConsultarDias();
					$Horarios = $this->Modelo->ConsultarHorario();
					$Instructores = AppUtilidades::ObtenerColumnaCoincidencia($Instructores, $Consulta, 'IdInformacion');
					$Periodos = AppUtilidades::ObtenerColumnaCoincidencia($Periodos, $Consulta, 'IdPeriodo');
					$Auxiliares = AppUtilidades::ObtenerColumnaCoincidencia($Auxiliares, $AuxiliaresSeleccionados, 'IdInformacion');
					$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
					$Validacion->Requerido('IdPeriodo', '* Campo Requerido');
					$Validacion->Requerido('Nombre', '* Campo Requerido');
					$Validacion->Requerido('NombreIcono', '* Campo Requerido');
					$Validacion->Requerido('Descripcion', '* Campo Requerido');
					$Validacion->Requerido('Lugar', '* Campo Requerido');
					$Validacion->Requerido('IdInstructor', '* Campo Requerido');
					$Validacion->CantMinCaracteres('Descripcion',150, '* Minimo 150 caracteres');
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Consulta', $Consulta);
					$Plantilla->Parametro('Instructores', $Instructores);
					$Plantilla->Parametro('Periodos', $Periodos);
					$Plantilla->Parametro('Auxiliares', $Auxiliares);
					$Plantilla->Parametro('HorarioTaller', $HorarioTaller);
					$Plantilla->Parametro('Dias', $Dias);
					$Plantilla->Parametro('Horarios', $Horarios);
					$Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
					$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarTaller'));
					$Plantilla->Filtro('Cifrado', function ($Parametro) {
						return NeuralCriptografia::Codificar($Parametro, APP);
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Editar', 'frmEditar.html')));
					unset($IdTaller, $Consulta, $Instructores, $Periodos, $Auxiliares, $HorarioTaller, $AuxiliaresSeleccionados, $Validacion, $Plantilla);
					exit();
				}
			}
		}

		/**
		 * Metodo Publico
		 * Editar()
		 *
		 * Funcion de editar taller
		 * @throws NeuralException
		 */
		public function Editar(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
					if (AppPost::DatosVaciosOmitidos($_POST, array('IdAuxiliares')) == false AND AppPost::DatosVaciosOmitidos($_POST, array('IdHorario')) == false AND AppPost::DatosVaciosOmitidos($_POST, array('IdDias')) == false) {
						if (isset($_POST['IdAuxiliares']) == true)
							$IdAuxiliares = $_POST['IdAuxiliares'];
						if (isset($_POST['IdDias']) == true)
							$IdDias = $_POST['IdDias'];
						if (isset($_POST['IdHorario']) == true)
							$IdHorarios = $_POST['IdHorario'];
						$IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
						$IdInstructor = NeuralCriptografia::DeCodificar($_POST['IdInstructor'], APP);
						unset($_POST['Key'], $_POST['IdAuxiliares'], $_POST['IdTaller'], $_POST['IdInstructor'], $_POST['IdDias'], $_POST['IdHorario']);
						$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
						$DatosPost['IdPeriodo'] = NeuralCriptografia::DeCodificar($DatosPost['IdPeriodo']);
						$Resultado = $this->Modelo->EditarDatos($DatosPost, $IdTaller);
						if (isset($Resultado) == true) {
							$this->Modelo->EliminarInstructoresTaller(array('IdTaller' => $IdTaller));
							$this->Modelo->EliminarHorarioTaller(array('IdTaller' => $IdTaller));
							$DatosInstructor = array(
								'IdInformacionInstructor' => $IdInstructor,
								'IdTaller' => $IdTaller,
								'InstructorPrincipal' => 'ACTIVO');
							$this->Modelo->GuardarInstructor($DatosInstructor);
							if (isset($IdAuxiliares) == true AND is_array($IdAuxiliares) == true AND count($IdAuxiliares) > 0) {
								foreach ($IdAuxiliares as $IdAuxiliar) {
									$this->Modelo->GuardarInstructor(array(
										'IdInformacionInstructor' => NeuralCriptografia::DeCodificar($IdAuxiliar, APP),
										'IdTaller' => $IdTaller));
								}
							}

							if (isset($IdDias) == true AND is_array($IdDias) == true AND count($IdDias) > 0){
								if (isset($IdHorarios) == true AND is_array($IdHorarios) == true AND count($IdHorarios) > 0){
									$PosicionIdDia = current($IdDias);
							         foreach ($IdHorarios as $IdHorario) {
										 $this->Modelo->GuardaTallerHorario(array(
											 'IdHorario' => NeuralCriptografia::DeCodificar($IdHorario, APP),
											 'IdDia' => NeuralCriptografia::DeCodificar($PosicionIdDia, APP),
											 'IdTaller' => $IdTaller));
										 $PosicionIdDia = next($IdDias);
									 }
							    }
							}
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Agregar', 'Exito.html')));
							unset($IdAuxiliares, $IdTaller, $IdInstructor, $DatosPost, $Resultado, $DatosInstructor, $IdAuxiliar, $Plantilla);
							exit();
						} else {
							// -- Mensaje de error al guardar datos
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Error', 'ErrorInsertandoDatos.html')));
							unset($IdAuxiliares, $IdTaller, $IdInstructor, $DatosPost, $Resultado, $Plantilla);
							exit();
						}
					} else {
						// -- Mensaje elementos requeridos para la captura
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Taller', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}
			}
		}
		/**
		 * Metodo Publico
		 * EliminarRegistro()
		 *
		 * Funcion Eliminar registro de taller
		 * @throws NeuralException
		 */
		public function EliminarRegistro(){
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
				if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
					$IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
					$this->Modelo->EliminarTaller($IdTaller);
				}
			}
		}
	}
