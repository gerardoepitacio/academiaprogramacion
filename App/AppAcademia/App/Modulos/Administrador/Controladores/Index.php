<?php

	class Index extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla Principal del sistema
		 *
		 */
		public function Index() {
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Principal', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}
        
        /** 
         *Metodo Publico
         * DataJson
         * 
         * Imprime una cadena en formato json 
         **/
        public function DataJson(){
            $Datos=$this->Modelo->ConsultarAsistenteTaller();
            $Matriz=null;
            for($i=0;$i<count($Datos);$i++){
                $Matriz[]=array($Datos[$i]['Nombre'],$Datos[$i]['Asistentes']);
            }
			echo json_encode($Matriz,JSON_NUMERIC_CHECK);                     
        }

	}