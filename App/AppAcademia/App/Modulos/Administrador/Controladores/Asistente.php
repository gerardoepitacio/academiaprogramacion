<?php

	class Asistente extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Listado de asistenets
		 *
		 */
		public function Index() {
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Asistente', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}
        
        public function frmListado(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                $Consulta = $this->Modelo->ConsultarAsistente();
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Filtro('Cifrado', function($Parametro){return NeuralCriptografia::Codificar($Parametro, APP);});
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Asistente', 'Listado', 'Listado.html')));
                unset($Consulta, $Plantilla);
                exit();
            }
        }

        /**
         * Metodo Publico
         * VerTalleres()
         *
         * ver talleres de asistente
         * @throws NeuralException
         */
        public function VerTalleres(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                $IdAsistente = NeuralCriptografia::DeCodificar($_POST['IdAsistente']);
                $Consulta = $this->Modelo->TalleresAsistente($IdAsistente);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Filtro('Cifrado', function($Parametro){return NeuralCriptografia::Codificar($Parametro, APP);});
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Asistente', 'Listado', 'ListadoTalleres.html')));
                unset($Consulta, $Plantilla);
                exit();
            }
        }
        
        /**
         * Metodo Publico
         * EliminarAsistente
         *
         * Recibe el arreglo post con el id del Asistente
         * y cambia su Status a "ELIMINADO"
         */
        public function EliminarAsistente(){
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
                if (isset($_POST) == true AND $_POST['IdUsuario'] != "") {
                    $IdAsistente = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                    $this->Modelo->EliminarAsistente($IdAsistente);
                }
            }
        }
        
        public function frmEditarAsistente(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
                if (isset($_POST) == true AND isset($_POST['IdUsuario']) == true AND $_POST['IdUsuario'] != '') {
                    $IdUsuario=NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                    unset($_POST);
                    $DatosUsuario=$this->Modelo->ConsultarInformacionUsuario($IdUsuario);
                    $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                    $Validacion->Requerido('Usuario', '* Campo requerido');
                    $Validacion->Requerido('Nombres','*Campo requerido');
                    $Validacion->Requerido('ApellidoPaterno', '* Campo requerido');
                    $Validacion->Requerido('ApellidoMaterno', '* Campo requerido');
                    $Validacion->Requerido('Correo', '* Campo requerido');
                    $Validacion->CantMaxCaracteres('Usuario',255, '* Máximo 255 caracteres');
                    $Validacion->CantMaxCaracteres('Password',255, '* Máximo 255 caracteres');
                    $Validacion->CantMaxCaracteres('RepitePassword',255, '* Máximo 255 caracteres');
                    $Validacion->CantMaxCaracteres('Nombres',255,'*Máximo 255 caracteres');
                    $Validacion->CantMaxCaracteres('ApellidoPaterno',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('ApellidoMaterno',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Direccion',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Cargo',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Correo',255,'* Máximo 255 Caracteres');
                    $Validacion->CampoIgual('RepitePassword','Password','* La contraseña no coincide con la anterior');
                    $Validacion->Email('Correo', '* Correo no válido');      
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    $Plantilla->Parametro('Consulta',$DatosUsuario);
                    $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                    $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarAsistente'));
                    $Plantilla->Filtro('Cifrado',function($parametros){return NeuralCriptografia::Codificar($parametros, APP);});
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Asistente', 'Editar', 'frmEditar.html')));
                }
            }
        }
        
        /**
         * Metodo publico
         * Actualizar()
         * Prepara los datos para editar toda la informacion del usuario
         * y hace la llamada a dicho metodo en el modelo
         */
        public function Actualizar(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                    $DatosPost=AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                    $IdUsuario= NeuralCriptografia::DeCodificar($DatosPost['IdUsuario'],APP);
                    unset($_POST,$DatosPost['Key'],$DatosPost['AuxiliarEstado'],$DatosPost['IdUsuario']);
                    $DatosPost['Status'] = (isset($DatosPost['Status'])==true) ? 'ACTIVO': 'DESACTIVADO';
                    $DatosPost=AppPost::FormatoEspacio(AppPost::ConvertirTextoUcwordsOmitido($DatosPost,array('Password','RepitePassword','Usuario','IdUsuario','Correo','Status')));
                    if($DatosPost['Password']==$DatosPost['RepitePassword'] AND $DatosPost['Password']!=''){
                        $this->Modelo->ActualizarPasswordUsuarioAsistente(array("Usuario" => $DatosPost['Usuario'],"Password" =>hash('sha256',$DatosPost['Password']),"Status"=>$DatosPost['Status']),$IdUsuario);
                    }else{
                        $this->Modelo->ActualizarUsuarioAsistente(array("Usuario" => $DatosPost['Usuario'],"Status"=>$DatosPost['Status']),$IdUsuario);
                    }
                    unset($DatosPost['Usuario'],$DatosPost['Password'],$DatosPost['RepitePassword'],$DatosPost['Status']);
                    $this->Modelo->ActualizarInformacionAsistente($DatosPost,$IdUsuario);
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Asistente', 'Editar', 'Exito.html')));
                    unset($Plantilla);
                    exit();
                }
            }
        }

	}