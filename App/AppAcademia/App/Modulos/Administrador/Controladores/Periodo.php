<?php
class Periodo extends Controlador
{

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del controlador Periodo
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista los Periodos no eliminados
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarPeriodos();
            $Plantilla = new NeuralPlantillasTwig(APP);

            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar Periodo.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Nombre', '* Campo Requerido');
            $Validacion->Requerido('FechaInicio', '* Campo Requerido');
            $Validacion->Requerido('FechaFin', '* Campo Requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarPeriodo'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Agregar', 'frmAgregar.html')));
            unset($Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Periodo
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                unset($_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $this->Modelo->GuardarDatos($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Agregar', 'Exito.html')));
                unset($DatosPost, $Plantilla);
                exit();
            }
        }
    }
    /**
     * Metodo Publico
     * frmEditar()
     *
     * Formulario para editar Periodo.
     * @throws NeuralException
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdPeriodo']) == true AND $_POST['IdPeriodo'] != '') {
                $IdPeriodo = NeuralCriptografia::DeCodificar($_POST['IdPeriodo'], APP);
                $Consulta = $this->Modelo->ConsultarPeriodos(array('IdPeriodo' => $IdPeriodo));
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Nombre', '* Campo Requerido');
                $Validacion->Requerido('FechaInicio', '* Campo Requerido');
                $Validacion->Requerido('FechaFin', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);

                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarTaller'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Editar', 'frmEditar.html')));
                unset($IdPeriodo, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Funcion de editar Periodo
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                    $IdPeriodo = NeuralCriptografia::DeCodificar($_POST['IdPeriodo'], APP);
                    unset($_POST['IdPeriodo'],$_POST['Key']);
                    $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                    $this->Modelo->EditarDatos($DatosPost, $IdPeriodo);
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Periodo', 'Agregar', 'Exito.html')));
                        unset($IdPeriodo, $DatosPost, $Plantilla);
                        exit();
            }
        }
    }

    /**
     * Metodo Publico
     * EliminarRegistro()
     *
     * Funcion Eliminar registro de Periodo
     * @throws NeuralException
     */
    public function EliminarRegistro(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdPeriodo']) == true AND $_POST['IdPeriodo'] != '') {
                $IdPeriodo = NeuralCriptografia::DeCodificar($_POST['IdPeriodo'], APP);
                $this->Modelo->EliminarPeriodo($IdPeriodo);
            }
        }
    }

   


}
    ?>