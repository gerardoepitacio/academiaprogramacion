<?php

class ActivacionCuentas extends Controlador{

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct(){
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Listado de asistenets
     *
     */
    public function Index(){
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ActivacionCuentas', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    public function ListadoActivaciones(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarActivacionesCuentas();
            if (count($Consulta) > 0) {
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro("Consulta", $Consulta);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ActivacionCuentas', 'Listado', 'Listado.html')));
            }
        }
    }

    public function frmActivarCuenta(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdUsuario']) == true AND empty($_POST['IdUsuario']) == false) {
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro("IdUsuario", $_POST['IdUsuario']);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ActivacionCuentas', 'Activacion', 'frmActivarCuenta.html')));
            }
        }
    }
    
    public function Activar(){
        Ayudas::print_r($_POST);
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                $DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
                $DatosPost['Status'] = (isset($DatosPost['Status']))? "ACTIVO" : "PROCESANDO";
                Ayudas::print_r($DatosPost);
                exit();
                $this->Modelo->ActivarCuenta($DatosPost['Status'], NeuralCriptografia::DeCodificar($DatosPost['IdUsuario']));
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ActivacionCuentas', 'Activacion', 'Exito.html')));
            }
        }
    }
}


