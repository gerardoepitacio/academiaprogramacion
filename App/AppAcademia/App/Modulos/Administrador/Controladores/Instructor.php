<?php

	class Instructor extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct() {
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla Principal del sistema
		 *
		 */
		public function Index() {
			$MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('Usuario', $Usuario);
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Index.html')));
			unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
			exit();
		}


        /**
         * Metodo Publico
         * frmListado()
         *
         * Lista todos los instructores registrados en la db
         */
        public function frmListado(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                $Consulta = $this->Modelo->ConsultarInstructores();
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Filtro('Cifrado', function($Parametro){
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Listado', 'Listado.html')));
                unset($Consulta, $Plantilla);
                exit();
            }
        }

        /**
         * Metodo publico
         * frmAgregar()
         *
         * Formulario para agregar un instructor.
         * @throws NeuralException
         */
        public function frmAgregar(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Usuario', '* Campo requerido');
                $Validacion->Requerido('Password', '* Campo requerido');
                $Validacion->Requerido('RepitePassword', '* Campo requerido');
                $Validacion->Requerido('Nombres','*Campo requerido');
                $Validacion->Requerido('ApellidoPaterno', '* Campo requerido');
                $Validacion->Requerido('ApellidoMaterno', '* Campo requerido');
                $Validacion->Requerido('Correo', '* Campo requerido');
                $Validacion->CantMaxCaracteres('Usuario',255, '* Máximo 255 caracteres');
                $Validacion->CantMaxCaracteres('Password',255, '* Máximo 255 caracteres');
                $Validacion->CantMaxCaracteres('RepitePassword',255, '* Máximo 255 caracteres');
                $Validacion->CantMaxCaracteres('Nombres',255,'*Máximo 255 caracteres');
                $Validacion->CantMaxCaracteres('ApellidoPaterno',255, '* Máximo 255 Caracteres');
                $Validacion->CantMaxCaracteres('ApellidoMaterno',255, '* Máximo 255 Caracteres');
                $Validacion->CantMaxCaracteres('Direccion',255, '* Máximo 255 Caracteres');
                $Validacion->CantMaxCaracteres('Cargo',255, '* Máximo 255 Caracteres');
                $Validacion->CantMaxCaracteres('Correo',255,'* Máximo 255 Caracteres');
                $Validacion->CampoIgual('RepitePassword','Password','* La contraseña no coincide con la anterior');
                $Validacion->Email('Correo', '* Correo no válido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarInstructor'));
                $Plantilla->Filtro('Cifrado', function($Parametro){
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Agregar', 'frmAgregar.html')));
                unset($Validacion,$Plantilla);
                exit();
            }
        }

        /**
         * Metodo publico
         * Actualizar()
         * Prepara los datos para editar la informacion del usuario
         * y hace la llamada a dicho metodo en el modelo
         */
        public function Actualizar(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                    $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                    unset($_POST,$DatosPost['Key'],$DatosPost['AuxiliarEstado']);
                    $DatosPost['Status'] = (isset($DatosPost['Status'])==true) ? 'ACTIVO': 'DESACTIVADO';
                    $DatosPost = AppPost::FormatoEspacio(AppPost::ConvertirTextoUcwordsOmitido($DatosPost,array('IdInformacion', 'IdUsuario','Correo','Status', 'Descripcion', 'UrlFacebook')));
                    $IdInformacion = NeuralCriptografia::DeCodificar($DatosPost['IdInformacion'],APP);
                    $IdUsuario = NeuralCriptografia::DeCodificar($DatosPost['IdUsuario'],APP);
                    $DatosEstado = array('Status' => $DatosPost['Status']);
                    $this->Modelo->ActualizarDatosUsuario($DatosEstado, $IdUsuario);
                    unset($DatosPost['IdInformacion'], $DatosPost['IdUsuario']);
                    $this->Modelo->ActualizarInformacionUsuario($DatosPost, $IdInformacion);
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Agregar', 'Exito.html')));
                    unset($Plantilla);
                    exit();
                }
            }
        }

        /**
         * Metodo publico
         * frmEditarInstructor()
         *
         * Formulario para editar la informacion de un instructor.
         * @throws NeuralException
         */
        public function frmEditarInstructor(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
                if (isset($_POST) == true AND isset($_POST['IdUsuario']) == true AND $_POST['IdUsuario'] != '') {
                    $IdUsuario=NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                    unset($_POST);
                    $DatosUsuario = $this->Modelo->ConsultarInformacionUsuario($IdUsuario);
                    $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                    $Validacion->Requerido('Nombres','*Campo requerido');
                    $Validacion->Requerido('ApellidoPaterno', '* Campo requerido');
                    $Validacion->Requerido('ApellidoMaterno', '* Campo requerido');
                    $Validacion->Requerido('Correo', '* Campo requerido');
                    $Validacion->CantMaxCaracteres('Nombres',255,'*Máximo 255 caracteres');
                    $Validacion->CantMaxCaracteres('ApellidoPaterno',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('ApellidoMaterno',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Direccion',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Cargo',255, '* Máximo 255 Caracteres');
                    $Validacion->CantMaxCaracteres('Correo',255,'* Máximo 255 Caracteres');
                    $Validacion->Email('Correo', '* Correo no válido');      
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    $Plantilla->Parametro('Consulta',$DatosUsuario);
                    $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                    $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarInstructor'));
                    $Plantilla->Filtro('Cifrado',function($parametros){return NeuralCriptografia::Codificar($parametros, APP);});
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Editar', 'frmEditarInstructor.html')));
                }
            }
            
        }
        
        /**
         * Metodo publico
         * Agregar()
         *
         * Metodo para registrar un nuevo instructor.
         * @throws NeuralException
         */
        public function Agregar(){
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
                if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                    $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                    unset($_POST,$DatosPost['Key']);
                    if($DatosPost['Password'] == $DatosPost['RepitePassword']){
                        if($this->Modelo->BuscarUsuario($DatosPost['Usuario']) == false){
                            $DatosPost = AppPost::FormatoEspacio(AppPost::ConvertirTextoUcwordsOmitido($DatosPost,array('Usuario','Password','RepitePassword','Correo','Status', 'Descripcion', 'UrlFacebook')));
                            $DatosUsuario=array('IdPerfil'=> 2, 'Usuario'=>$DatosPost['Usuario'],'Password'=>hash('sha256',$DatosPost['Password']),'Status'=>$DatosPost['Status']);
                            unset($DatosPost['Usuario'], $DatosPost['Password'], $DatosPost['RepitePassword']);
                            $this->Modelo->InsertarInformacionUsuario($DatosPost, $this->Modelo->InsertarUsuario($DatosUsuario));
                            unset($DatosPost,$DatosUsuario);
                            $Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Agregar', 'Exito.html')));
                            unset($Plantilla);
							exit();
                        }else{
                            $Plantilla = new NeuralPlantillasTwig(APP);
    						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Error', 'ErrorUsuario.html')));
    						unset($Plantilla);
    						exit();
                        }
                    }else{
                        $Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Error', 'ErrorPassword.html')));
						unset($Plantilla);
						exit();
                    }
                }else{
                    $Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Error', 'ErrorElementosRequeridos.html')));
					unset($Plantilla);
					exit();
                }
            }
        }

        /**
         * Metodo publico
         * ListarTalleresInstructor()
         *
         * Genera una vista donde se ven los talleres asociados al instructor
         */

        public function ListarTalleresInstructor(){
            if(isset($_POST['IdUsuario']) AND $_POST['IdUsuario'] != "") {
                $Datos = $this->Modelo->ListarTalleresInstructor(NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Filtro('Cifrado',function($parametros){return NeuralCriptografia::Codificar($parametros);});
                $Plantilla->Parametro('TalleresAsociados', $Datos);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Listado', 'ListaTalleres.html')));
                unset($Datos,$Plantilla);
            }
        }

        /**
         * Metodo Publico
         * ListarAsistentesTaller()
         *
         * Recibe el arreglo Post y genera la vista
         * para los participantes de un Taller seleccionado
         */
        

        public function ListarAsistentesTaller(){
            if(isset($_POST['IdTaller']) AND $_POST['IdTaller'] != "") {
                $Datos = $this->Modelo->ListarAsistentesTaller(NeuralCriptografia::DeCodificar($_POST['IdTaller'],APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Filtro('Cifrado',function($parametros){return NeuralCriptografia::Codificar($parametros);});
                $Plantilla->Parametro('ListaAsistentes', $Datos);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Instructor', 'Listado', 'ListaAsistentes.html')));
                unset($Datos, $Plantilla);
            }
        }

        /**
         * Metodo Publico
         * EliminarInstructor()
         *
         * Recibe el arreglo post con el id del instructor
         * y cambia su Status a "ELIMINADO"
         */

        public function EliminarInstructor(){
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
                if (isset($_POST) == true AND $_POST['IdUsuario'] != "") {
                    $IdInstructor = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                    $this->Modelo->EliminarInstructor($IdInstructor);
                }
            }
        }


	}