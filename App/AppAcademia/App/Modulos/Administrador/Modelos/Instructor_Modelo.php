<?php
	
	/**
	 * Clase: Asesor_Modelo
	 */
	class Instructor_Modelo extends AppSQLConsultas  {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo: Ejemplo
		 */
		public function ConsultaSQL() {
		}
        
        /**
         * Metodo Publico
         * ConsultarInstructores()
         *
         * Consulta y retorna a los usuarios de perfil Instructor y que aparte estos esten activos 
         * dentro de la Base de Datos
         */
        public function ConsultarInstructores() {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_sistema_usuarios');
            $Consulta->Columnas("tbl_sistema_usuarios.IdUsuario,Nombres,ApellidoPaterno,ApellidoMaterno,TelefonoMovil1,Correo,tbl_sistema_usuarios.Status");
            $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
            $Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
            $Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Instructor'");
            $Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
            return $Consulta->Ejecutar(false,true);
        }
        
        /**
         * @param var $Datos
         * Metodo Publico InsertarUsuario
         * Recibe un arreglo para con el nombre y la contraseña para registrar un nuevo usuario
         * */  
        public function InsertarUsuario($Datos){
            if($Datos == true AND is_array($Datos) == true){
				try{
					$this->Conexion->insert('tbl_sistema_usuarios', $Datos);
					return $this->Conexion->lastInsertId();
				} catch (PDOException $e){} catch (Exception $e) {}
			}
        }
        
        /**
         * @param var $Usuario
         * Metodo Publico BuscarUsuario
         * Recibe el nombre del usuario para buscar si este esta registrado y devuelve el Id
         * */  
        public function BuscarUsuario($Usuario=false){
            if(isset($Usuario) == true AND $Usuario != ""){
                $Consulta = new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_sistema_usuarios');
                $Consulta->Columnas("tbl_sistema_usuarios.IdUsuario");
                $Consulta->Condicion("tbl_sistema_usuarios.Usuario = '".$Usuario."'");
                return $Consulta->Ejecutar(false,true);
            }
        }

        /**
         * @param bool $IdUsuario
         * @return array
         *
         * Metodo Publico
         * ListarTalleresInstructor();
         * Recibe el Id de un usuario seleccionado de la vista de instructores
         * para posteriormente consultar los talleres asociados a el
         */
        public function ListarTalleresInstructor($IdUsuario = false){
            if(isset($IdUsuario) == true AND $IdUsuario != ""){
                $Consulta=new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_talleres');
                $Consulta->Columnas(implode(',', self::ListarColumnas('tbl_talleres', false, false, APP)));
                $Consulta->InnerJoin('tbl_instructores_talleres', 'tbl_talleres.IdTaller', 'tbl_instructores_talleres.IdTaller');
                $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_instructores_talleres.IdInformacionInstructor', 'tbl_informacion_usuarios.IdInformacion');
                $Consulta->InnerJoin('tbl_sistema_usuarios', 'tbl_informacion_usuarios.IdUsuario', 'tbl_sistema_usuarios.IdUsuario');
                $Consulta->Condicion("tbl_sistema_usuarios.IdUsuario = '$IdUsuario'");
                return $Consulta->Ejecutar(false,true);
            }
        }

        /**
         * @param bool $IdTaller
         * @return array
         *
         * Metodo Publico
         * ListarAsistentesTaller
         * Recibe el Id de un Taller seleccionado de la vista correspondiente
         * para consultar los asistentes a este mismo.
         */
        public function ListarAsistentesTaller($IdTaller = false){
            if(isset($IdTaller)== true AND $IdTaller != ""){
                $Consulta=new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_talleres_asistentes');
                $Consulta->Columnas("Nombres,ApellidoPaterno,ApellidoMaterno,TelefonoMovil1,Correo,CalificacionAsistente,Status");
                $Consulta->InnerJoin('tbl_informacion_usuarios','tbl_talleres_asistentes.IdInformacionAsistente',
                    'tbl_informacion_usuarios.IdUsuario');
                $Consulta->Condicion("tbl_talleres_asistentes.IdTaller = '$IdTaller'");
                return $Consulta->Ejecutar(false, true);
            }
        }
        
        /**
         * @param array $Array, var $IdUsuario
         * Metodo Publico InsertarInformacionUsuario
         * Registra la informacion de un usuario y lo asocia a su Id
         * */ 
        public function InsertarInformacionUsuario($Array = false, $IdUsuario=false){
            if($Array == true AND $IdUsuario == true){
                $SQL = new NeuralBDGab(APP, 'tbl_informacion_usuarios');
                $SQL->Sentencia('IdUsuario',$IdUsuario);
                foreach ($Array as $key => $Valor){
                    $SQL->Sentencia($key, $Valor);
                }
                $SQL->Insertar();
            }            
        }
        
        /**
         * @param var $IdUsuario
         * Metodo Publico ConsultarInformacionUsuario
         * Recupera la informacion de un usuario con un determinado Id
         * */
        public function ConsultarInformacionUsuario($IdUsuario=false){
            if(isset($IdUsuario)==true AND $IdUsuario!=""){           
                $Consulta=new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_informacion_usuarios');
                $Consulta->Columnas("IdInformacion,tbl_informacion_usuarios.IdUsuario,Nombres,ApellidoPaterno,ApellidoMaterno,Direccion,Cargo,Correo,TelefonoFijo1,TelefonoMovil1,tbl_sistema_usuarios.Status, Descripcion, UrlFacebook");
                $Consulta->InnerJoin('tbl_sistema_usuarios','tbl_informacion_usuarios.IdUsuario','tbl_sistema_usuarios.IdUsuario');
                $Consulta->Condicion('tbl_informacion_usuarios.IdUsuario = '.$IdUsuario);
                return $Consulta->Ejecutar(false,true);      
            }            
        }

        /**
         * @param bool $IdInstructor
         *
         * Metodo Publico Eliminar Instructor
         * Cambia el Status del instructor asociado al id
         * por "ELIMINADO"
         */

        public function EliminarInstructor($IdInstructor = false){
            if($IdInstructor == true AND $IdInstructor != ''){
                try{
                    $this->Conexion->update('tbl_sistema_usuarios',array('Status'=>"ELIMINADO"), array('IdUsuario'=>$IdInstructor));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }

        /**
         * @param bool $Datos
         * @param bool $IdInformacion
         *
         * Metodo publico
         * ActualizarInformacionUsuario()
         *
         * Recibe un array y el Id de la informacion del usuario que se modificara
         */
        public function ActualizarInformacionUsuario($Datos = false, $IdInformacion=false){
            if($IdInformacion == true AND $IdInformacion != '' AND is_array($Datos) == true){
                try{
                    $this->Conexion->update('tbl_informacion_usuarios', $Datos, array('IdInformacion'=>$IdInformacion));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }

        /**
         * @param bool $Datos
         * @param bool $IdUsuario
         *
         * Metodo publico
         * ActualizarDatosUsuario()
         *
         * Recibe  un array con el Status del usuario y su id para modificarlo en la DB
         */
        public function ActualizarDatosUsuario($Datos = false, $IdUsuario = false){
            if($IdUsuario == true AND $IdUsuario != '' AND is_array($Datos) == true){
                try{
                    $this->Conexion->update('tbl_sistema_usuarios', $Datos, array('IdUsuario'=>$IdUsuario));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }
	}