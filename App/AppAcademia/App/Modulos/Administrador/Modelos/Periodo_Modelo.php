<?php

/**
 * Clase: Periodo_Modelo
 */
class Periodo_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarPeriodos()
     *
     * Consulta todos los Horarios
     * @return mixed
     */
    public function ConsultarPeriodos($Condiciones = false){
        $SQL = "SELECT * FROM tbl_periodos".
            " WHERE Status = 'ACTIVO'";
        if($Condiciones == true AND is_array($Condiciones))
            $SQL.= ' AND '.self::ObtenerCondicionesAND($Condiciones);
        $SQL .= " ORDER BY FechaInicio DESC";
        $Consulta=$this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * GuardarDatos($ArregloDatos = false)
     *
     * Guarda el Periodo
     * @param bool $Datos: Arreglo de datos, DatosPost
     * @return mixed
     */
    public function GuardarDatos($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_periodos', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EditarDatos($Datos = false, $IdPeriodo = false)
     *
     * Editar datos del Periodo
     * @param bool $Datos
     * @param bool $IdPeriodo
     */
    public function EditarDatos($Datos = false, $IdPeriodo = false){
        if($Datos == true AND is_array($Datos) == true AND $IdPeriodo == true AND $IdPeriodo != ''){
            try{
                return $this->Conexion->update('tbl_periodos', $Datos, array('IdPeriodo'=>$IdPeriodo));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EliminarPeriodo($IdPeriodo = false)
     *
     * Actualizamos el estado del Periodo a DESACTIVADO
     * para que no se muestre
     * @param bool $IdPeriodo
     */
    public function EliminarPeriodo($IdPeriodo = false){
        if($IdPeriodo == true AND $IdPeriodo != ''){
            try{
                $this->Conexion->update('tbl_periodos',array('Status'=>"ELIMINADO"), array('IdPeriodo'=>$IdPeriodo));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    
}

?>