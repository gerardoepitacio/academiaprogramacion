<?php
	
	/**
	 * Clase: Taller_Modelo
	 */
	class Taller_Modelo extends AppSQLConsultas{
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarTalleres($Condiciones = false)
		 *
		 * Lista talleres no eliminados, con su periodo e instructor principal
		 * Metodo: Ejemplo
		 */
		public function ConsultarTalleres($Condiciones = false) {
			$Campos = implode(',', self::ListarColumnas('tbl_talleres', false, false, APP));
			$Campos.= ', '.implode(',', self::ListarColumnas('tbl_periodos', false, array('Nombre'=>'Periodo'), APP));
			$Campos.=', tbl_informacion_usuarios.IdInformacion, tbl_informacion_usuarios.Nombres AS Instructor';
			$SQL = "SELECT $Campos FROM tbl_talleres".
				" INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo".
				" INNER JOIN tbl_instructores_talleres ON tbl_talleres.IdTaller = tbl_instructores_talleres.IdTaller".
				" INNER JOIN tbl_informacion_usuarios ON tbl_instructores_talleres.IdInformacionInstructor = tbl_informacion_usuarios.IdInformacion ".
				" WHERE tbl_talleres.Status != 'ELIMINADO' AND tbl_instructores_talleres.InstructorPrincipal = 'ACTIVO'";
			if($Condiciones == true AND is_array($Condiciones))
				$SQL.= ' AND '.self::ObtenerCondicionesAND($Condiciones);

			$SQL .= " ORDER BY tbl_talleres.FechaHoraCreacion DESC";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultaDias()
		 *
		 * Consulta los dias
		 * @return mixed
		 */
		public function ConsultarDias(){
			$SQL = "SELECT IdDia, NombreDia FROM tbl_dias";
			$Consulta=$this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 *  ConsultarDiasHorarios($Condicion = false)
		 *
		 * Consulta los dias y el horario correspondiente de un taller
		 * @param bool $Condicion
		 * @return mixed
		 */
		public function ConsultarDiasHorarios($Condicion = false){
			$Campos="NombreDia, HoraInicio, HoraFin, tbl_taller_horarios.IdDia, tbl_taller_horarios.IdHorario";
			$SQL = "SELECT $Campos FROM tbl_taller_horarios".
				   " INNER JOIN tbl_dias ON tbl_taller_horarios.IdDia = tbl_dias.IdDia".
				   " INNER JOIN tbl_horarios ON tbl_taller_horarios.IdHorario = tbl_horarios.IdHorario";
			$SQL.= " WHERE tbl_taller_horarios.IdTaller = $Condicion";
			$SQL.= " ORDER BY tbl_dias.IdDia";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultarHorario()
		 *
		 * Consulta todos los Horarios
		 * @return mixed
		 */
		public function ConsultarHorario(){
			$SQL = "SELECT IdHorario, HoraInicio, HoraFin FROM tbl_horarios";
			$Consulta=$this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultaAuxiliaresTaller($Condiciones = false)
		 *
		 * Consulta los auxiliares del taller seleccionado.
		 * @param bool $Condiciones
		 * @return mixed
		 */
		public function ConsultaAuxiliaresTaller($Condiciones = false){
			if($Condiciones == true AND is_array($Condiciones) == true){
				$Campos = 'tbl_informacion_usuarios.IdInformacion, tbl_informacion_usuarios.Nombres AS Auxiliar';
				$SQL = "SELECT $Campos FROM tbl_informacion_usuarios".
					" INNER JOIN tbl_instructores_talleres ON tbl_informacion_usuarios.IdInformacion = tbl_instructores_talleres.IdInformacionInstructor".
					" INNER JOIN tbl_talleres ON tbl_instructores_talleres.IdTaller = tbl_talleres.IdTaller".
					" WHERE tbl_talleres.Status != 'ELIMINADO' AND tbl_instructores_talleres.InstructorPrincipal = 'INACTIVO'";
				if($Condiciones == true AND is_array($Condiciones))
					$SQL.= ' AND '.self::ObtenerCondicionesAND($Condiciones);
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarInstructores($IdInstructorSeleccionado = false)
		 *
		 * Lista los instructores no eliminados
		 * @param bool $IdInstructorSeleccionado: Id del instructor que no va a ser seleccionado.
		 * @return mixed
		 */
		public function ConsultarInstructores($IdInstructorSeleccionado = false) {
			$Campos = implode(',', self::ListarColumnas('tbl_informacion_usuarios', false, false, APP));
			$SQL = "SELECT $Campos FROM tbl_informacion_usuarios".
				" INNER JOIN tbl_sistema_usuarios ON tbl_informacion_usuarios.IdUsuario = tbl_sistema_usuarios.IdUsuario".
				" INNER JOIN tbl_sistema_usuarios_perfil on tbl_sistema_usuarios.IdPerfil = tbl_sistema_usuarios_perfil.IdPerfil".
				" WHERE tbl_informacion_usuarios.Status != 'ELIMINADO' and tbl_sistema_usuarios_perfil.IdPerfil=2";
			if($IdInstructorSeleccionado == true AND $IdInstructorSeleccionado != '')
				$SQL = $SQL." AND tbl_informacion_usuarios.IdInformacion != $IdInstructorSeleccionado";
			$SQL .= " ORDER BY tbl_informacion_usuarios.ApellidoPaterno DESC";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultarPeriodos()
		 *
		 * Devuelve los periodos activos.
		 * @return mixed
		 */
		public function ConsultarPeriodos() {
			$Campos = implode(',', self::ListarColumnas('tbl_periodos', false, false, APP));
			$SQL = "SELECT $Campos FROM tbl_periodos".
				" WHERE tbl_periodos.Status = 'ACTIVO'";
			$SQL .= " ORDER BY tbl_periodos.FechaInicio DESC";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * GuardarDatos($ArregloDatos = false)
		 *
		 * Guarda el taller
		 * @param bool $Datos: Arreglo de datos, DatosPost
		 * @return mixed
		 */
		public function GuardarDatos($Datos = false){
			if($Datos == true AND is_array($Datos) == true){
				try{
					$this->Conexion->insert('tbl_talleres', $Datos);
					return $this->Conexion->lastInsertId();
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

		/**
		 * Metodo Publico
		 * GuardarInstructor($Datos = false)
		 *
		 * Asocia instructores con un taller.
		 * @param bool $Datos
		 * @return mixed
		 */
		public function GuardarInstructor($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            $this->Conexion->insert('tbl_instructores_talleres', $Datos);
            return $this->Conexion->lastInsertId();
        }
    }

		/**
		 * Metodo Publico
		 * GuardaHorario($Datos = false)
		 *
		 * Guarda un horario
		 * @param bool $Dato
		 * @return mixed
		 */
		public function GuardaHorario($Datos = false){
			if($Datos == true AND is_array($Datos) == true){
				$this->Conexion->insert('tbl_horarios',$Datos);
				return $this->Conexion->lastInsertId();
			}
		}

		/**
		 * Metodo Publico
		 * GuardaHorario($Datos = false)
		 *
		 * Guarda el horario del taller
		 * @param bool $Datos
		 */
		public function GuardaTallerHorario($Datos = false){
			if($Datos == true AND is_array($Datos) == true){
				$this->Conexion->insert('tbl_taller_horarios',$Datos);
				return $this->Conexion->lastInsertId();
			}
		}

		/**
		 * Metodo Publico
		 * EditarDatos($Datos = false, $IdTaller = false)
		 *
		 * Editar datos del taller
		 * @param bool $Datos
		 * @param bool $IdTaller
		 */
		public function EditarDatos($Datos = false, $IdTaller = false){
			if($Datos == true AND is_array($Datos) == true AND $IdTaller == true AND $IdTaller != ''){
				try{
					return $this->Conexion->update('tbl_talleres', $Datos, array('IdTaller'=>$IdTaller));
				} catch (PDOException $e) {
				} catch (Exception $e) {
				}
			}
		}

		/**
		 * Metodo Publico
		 * EliminarInstructoresTaller($Condiciones = false)
		 *
		 * Elimina los instructores de acuerdo a las condiciones
		 * @param bool $Condiciones
		 */
		public function EliminarInstructoresTaller($Condiciones = false){
			if($Condiciones == true AND is_array($Condiciones)){
				$this->Conexion->delete('tbl_instructores_talleres', $Condiciones);
			}
		}

		/**
		 * Metodo Publico
		 * EliminarTaller($IdTaller = false)
		 *
		 * Actualizamos el estado del taller a DESACTIVADO
		 * para que no se muestre
		 * @param bool $IdTaller
		 */
		public function EliminarTaller($IdTaller = false){
			if($IdTaller == true AND $IdTaller != ''){
				try{
					$this->Conexion->update('tbl_talleres',array('Status'=>"ELIMINADO"), array('IdTaller'=>$IdTaller));
					} catch (PDOException $e) {
					} catch (Exception $e) {}
			}
		}

		/**
		 * Metodo Publico
		 * EliminarHorarioTaller($Condicion = false)
		 * Elimina los Horarios de los Talleres de acuerdo a la condicion
		 * @param bool $Condicion
		 */
		public function EliminarHorarioTaller($Condicion = false){
			if($Condicion == true AND is_array($Condicion)){
				$this->Conexion->delete('tbl_taller_horarios', $Condicion);
			}
		}

	}