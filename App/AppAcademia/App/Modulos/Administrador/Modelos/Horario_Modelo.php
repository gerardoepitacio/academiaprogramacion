<?php

/**
 * Clase: Horario_Modelo
 */
class Horario_Modelo extends AppSQLConsultas{

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarHorario()
     *
     * Consulta todos los Horarios o dependiendo de la condicion
     * @return mixed
     */
    public function ConsultarHorario($Condiciones = false){
        $SQL = "SELECT * FROM tbl_horarios";
        if($Condiciones == true AND is_array($Condiciones))
            $SQL .= ' WHERE ' . self::ObtenerCondicionesAND($Condiciones);
        $Consulta=$this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Metodo Publico
     * GuardaHorario($Datos = false)
     *
     * Guarda un horario
     * @param bool $Dato
     * @return mixed
     */
    public function GuardaHorario($Datos = false){
        if ($Datos == true AND is_array($Datos) == true) {
            try {
                $this->Conexion->insert('tbl_horarios', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * EditarDatos($Datos = false, $IdHorario = false)
     *
     * Editar datos del Horario
     * @param bool $Datos
     * @param bool $IdHorario
     */
    public function EditarDatos($Datos = false, $IdHorario = false){
        if($Datos == true AND is_array($Datos) == true AND $IdHorario == true AND $IdHorario != ''){
            try{
                return $this->Conexion->update('tbl_horarios', $Datos, array('IdHorario'=>$IdHorario));
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

}