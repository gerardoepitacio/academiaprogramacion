<?php

class ActivacionCuentas_Modelo extends Modelo{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    public function ConsultarActivacionesCuentas(){
        $SQL = "SELECT tbl_informacion_usuarios.IdUsuario, IdActivacion, Fecha_Validacion, tbl_informacion_usuarios.Correo, tbl_activacion_cuentas.Status FROM tbl_activacion_cuentas ".
            "INNER JOIN tbl_informacion_usuarios ON tbl_activacion_cuentas.IdUsuario = tbl_informacion_usuarios.IdUsuario ";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ActivarCuenta($Status = false, $Condicion = false){
        if($Status == true AND $Condicion == true){
            $SQL = new NeuralBDGab(APP, 'tbl_activacion_cuentas');
            $SQL->Sentencia('Status',$Status);
            $SQL->Condicion("IdUsuario", $Condicion);
            $SQL->Actualizar();
        }
    }
}