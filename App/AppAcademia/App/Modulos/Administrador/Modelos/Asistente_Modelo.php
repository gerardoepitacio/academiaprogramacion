<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Asistente_Modelo extends Modelo {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
            $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
        
        /**
         * Metodo publico
         * ConsultarAsistente
         * 
         * Lista todos los asistentes
         * */
        public function ConsultarAsistente(){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_sistema_usuarios');
            $Consulta->Columnas("tbl_informacion_usuarios.IdUsuario,tbl_sistema_usuarios.Usuario,tbl_sistema_usuarios.`Status` AS StatusUsuario,CONCAT(tbl_informacion_usuarios.Nombres,' ',tbl_informacion_usuarios.ApellidoPaterno,' ',tbl_informacion_usuarios.ApellidoMaterno) AS Nombres,tbl_informacion_usuarios.Correo,tbl_informacion_usuarios.`Status` AS StatusInformacion");
            $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
            $Consulta->Condicion('tbl_sistema_usuarios.IdPerfil=3');
            $Consulta->Condicion('tbl_sistema_usuarios.`Status`!="ELIMINADO"');
            $Consulta->Ordenar('tbl_informacion_usuarios.Nombres');
            return $Consulta->Ejecutar(false,true);
        }

        /**
         * Metodo Publico
         * TalleresAsistente($IdAsistente = false)
         *
         * Talleres de asistentes
         * @param bool $IdAsistente
         * @return mixed
         */
        public function TalleresAsistente($IdAsistente = false){
            if($IdAsistente == true) {
                $SQL = "SELECT tbl_talleres.Nombre FROM tbl_talleres_asistentes ".
                "INNER JOIN tbl_talleres ON tbl_talleres_asistentes.IdTaller = tbl_talleres.IdTaller ".
                "WHERE tbl_talleres_asistentes.IdInformacionAsistente = $IdAsistente GROUP BY tbl_talleres.Nombre";
                $Consulta = $this->Conexion->prepare($SQL);
                $Consulta->execute();
                return $Consulta->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        
        /**
         * Metodo publico
         * EliminarAsistente
         * 
         * Cambia el status del asistente como eliminado
         * */
        public function EliminarAsistente($IdUsuario = false){
            if($IdUsuario == true AND $IdUsuario != ''){
                try{
                    $this->Conexion->update('tbl_sistema_usuarios',array('Status'=>"ELIMINADO"),
                        array('IdUsuario'=>$IdUsuario));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }
        
        /**
         * @param var $IdUsuario
         * Metodo Publico ConsultarInformacionUsuario
         * Recupera la informacion de un usuario con un determinado Id
         * */
        public function ConsultarInformacionUsuario($IdUsuario=false){
            if(isset($IdUsuario)==true AND $IdUsuario!=""){           
                $Consulta=new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_informacion_usuarios');
                $Consulta->Columnas("IdInformacion,tbl_informacion_usuarios.IdUsuario,tbl_sistema_usuarios.Usuario,Nombres,ApellidoPaterno,ApellidoMaterno,Direccion,Cargo,Correo,TelefonoFijo1,TelefonoMovil1,tbl_sistema_usuarios.`Status`");
                $Consulta->InnerJoin('tbl_sistema_usuarios','tbl_informacion_usuarios.IdUsuario','tbl_sistema_usuarios.IdUsuario');
                $Consulta->Condicion('tbl_informacion_usuarios.IdUsuario = '.$IdUsuario);
                return $Consulta->Ejecutar(false,true);      
            }            
        }
        
        /**
         * Metodo publico
         * ActualizarUsuarioAsistente
         * 
         * Cambia el nombre del usuario de un asistente
         * */
        public function ActualizarUsuarioAsistente($Datos = false,$IdUsuario=false){
            if($Datos == true AND  isset($Datos)){
                try{
                    $this->Conexion->update('tbl_sistema_usuarios',$Datos,array('IdUsuario'=>$IdUsuario));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }
        
        /**
         * Metodo publico
         * ActualizarPasswordUsuarioAsistente
         * 
         * Cambia el nombre del usuario y el password de un determinado asistente
         * */
        public function ActualizarPasswordUsuarioAsistente($Datos = false,$IdUsuario=false){
            if($Datos == true AND  isset($Datos)){
                try{
                    $this->Conexion->update('tbl_sistema_usuarios',$Datos,array('IdUsuario'=>$IdUsuario));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }
        
         /**
         * Metodo publico
         * ActualizarInformacionAsistente
         * 
         * Cambia el nombre del usuario con perfil de asistente
         * */
        public function ActualizarInformacionAsistente($Datos = false,$IdUsuario=false){
            if($Datos == true AND  isset($Datos)){
                try{
                    $this->Conexion->update('tbl_informacion_usuarios',$Datos,array('IdUsuario'=>$IdUsuario));
                } catch (PDOException $e) {
                } catch (Exception $e) {}
            }
        }
	}