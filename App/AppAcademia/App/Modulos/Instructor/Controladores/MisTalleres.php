<?php

class MisTalleres extends Controlador
{

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del controlador MisTalleres
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista los talleres del Instructor
     */
    public function frmListado(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdInformacionInstructor=$this->Informacion['Informacion']['IdInformacion'];
            $Consulta = $this->Modelo->ConsultarTalleres(0,$IdInformacionInstructor);
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla, $IdInformacionInstructor);
            exit();
        }
    }

    /**
     * Metodo Publico
     * ConsultaDiasHorario()
     *
     * Lista el horario del taller
     * @throws NeuralException
     */
    public function ConsultaDiasHorario(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta=$this->Modelo->ConsultarDiasHorarios(NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP));
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('HorarioTaller', $Consulta);
            if (isset($Consulta) == true AND is_array($Consulta) == true AND count($Consulta) > 0) {
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'Modal', 'TablaHorarios.html')));
                unset($Consulta, $Plantilla);
                exit;
            }else{
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'Modal', 'SinHorario.html')));
                unset($Consulta, $Plantilla);
                exit;
            }
        }
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista las unidades con sus temas
     */
    public function frmListadoUnidades(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $IdInformacionInstructor = $this->Informacion['Informacion']['IdInformacion'];
                $Consulta = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller), $IdInformacionInstructor);
                $CosultaUnidades =  $this->Modelo->ConsultarUnidades($IdTaller);
                $CosultaTemas = $this->Modelo->ConsultarTemas($IdTaller);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Taller', $Consulta[0]);
                $Plantilla->Parametro('Unidades', $CosultaUnidades);
                $Plantilla->Parametro('Temas', $CosultaTemas);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'ListadoContenido', 'ListadoContenido.html')));
                unset($Consulta, $Plantilla, $IdTaller, $CosultaUnidades, $CosultaTemas, $IdInformacionInstructor);
                exit();
            }
        }
    }
    /**
     * Metodo Publico
     * frmAgregarUnidad()
     *
     * formulario para agregar unidad
     */
    public function frmAgregarUnidad(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
                    $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                    $IdInformacionInstructor = $this->Informacion['Informacion']['IdInformacion'];
                    $ConsultaTaller = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller), $IdInformacionInstructor);
                    $CosultaUnidades =  $this->Modelo->ConsultarUnidades($IdTaller);
                    $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                    $Validacion->Requerido('NombreUnidad', '* Campo Requerido');
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    $Plantilla->Parametro('Taller', $ConsultaTaller[0]);
                    $Plantilla->Parametro('Unidades',$CosultaUnidades);
                    $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                    $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarUnidad'));
                    $Plantilla->Filtro('Cifrado', function ($Parametro) {
                        return NeuralCriptografia::Codificar($Parametro, APP);
                    });
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'frmAgregar.html')));
                    unset($IdTaller, $Validacion, $Plantilla, $ConsultaTaller);
                    exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Agregar()
     *
     * Funcion de agregar Unidad
     * @throws NeuralException
     */
    public function Agregar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                unset($_POST['Key'], $_POST['IdTaller']);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('IdTaller', $IdTaller);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                if (isset($_POST) AND isset($_POST['NombreUnidad']) == true) {
                    $DatosPost = $_POST["NombreUnidad"];
                    foreach ($DatosPost as $Unidad) {
                        $this->Modelo->GuardaUnidad(array("IdTaller" => $IdTaller, "NombreUnidad" => $Unidad));
                    }
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Exito.html')));
                    unset($DatosPost, $IdTaller, $Plantilla);
                    exit();                    
                }else{
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Error.html')));
                    unset($DatosPost, $IdTaller, $Plantilla);
                    exit();
                }
            }
        }
    }
    /**
     * Metodo Publico
     * frmEditarUnidad()
     *
     * formulario para agregar unidad
     */
    public function frmEditar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $Consulta=$this->Modelo->ConsultarUnidades($IdTaller);
                $IdInformacionInstructor = $this->Informacion['Informacion']['IdInformacion'];
                $ConsultaTaller = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller), $IdInformacionInstructor);
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('NombreUnidad', '* Campo Requerido');
                $NCampo=count($Consulta);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('NCampo', $NCampo);
                $Plantilla->Parametro('Taller', $ConsultaTaller[0]);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarUnidad'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'EditarUnidad', 'frmEditar.html')));
                unset($IdTaller, $Validacion, $Plantilla, $IdInformacionInstructor, $ConsultaTaller);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Actualizamos los datos
     * @throws NeuralException
     */
    public function Editar(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $Plantilla = new NeuralPlantillasTwig(APP);
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $Plantilla->Parametro('IdTaller', $IdTaller);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                unset($_POST['Key'], $_POST['IdTaller']);
                if(count($_POST) > 0){
                    $IdUnidad= $_POST['IdUnidad'];
                    $DatosPost = $_POST["NombreUnidad"];
                    $Posicion = current($DatosPost);
                    foreach ($IdUnidad as $Unidad) {
                        $this->Modelo->EditaUnidad(array("NombreUnidad"=>$Posicion), NeuralCriptografia::DeCodificar($Unidad, APP));
                        $Posicion = next($DatosPost);
                    }
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Exito.html')));
                    unset($DatosPost, $IdTaller, $IdUnidad, $Plantilla, $Posicion);
                    exit();
                }else{
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Error.html')));
                    unset($DatosPost, $IdTaller, $IdUnidad, $Plantilla, $Posicion);
                    exit();
                }
            }
        }
    }

    /**
     * Metodo Publico
     * frmEditarTema()
     *
     * formulario para modificar o agregar temas a la Unidad
     */
    public function frmEditarTema(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdUnidad']) == true AND $_POST['IdUnidad'] != '') {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $IdUnidad = NeuralCriptografia::DeCodificar($_POST['IdUnidad'], APP);
                $IdInformacionInstructor = $this->Informacion['Informacion']['IdInformacion'];
                $ConsultaTaller = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller), $IdInformacionInstructor);
                $Consulta=$this->Modelo->ConsultarTemasUnidades($IdUnidad);
                $ConsultaUnidad=$this->Modelo->ConsultarUnidad($IdUnidad);
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('NombreTema', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('Taller', $ConsultaTaller[0]);
                $Plantilla->Parametro('Unidad', $ConsultaUnidad[0]);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarTema'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'EditarTemas', 'frmEditar.html')));
                unset($IdTaller, $Validacion, $Plantilla, $IdUnidad, $IdInformacionInstructor, $ConsultaTaller, $Consulta);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * Editar()
     *
     * Actualizamos los temas
     * @throws NeuralException
     */
    public function EditarTema(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $IdUnidad = NeuralCriptografia::DeCodificar($_POST['IdUnidad'], APP);
                unset($_POST['Key'], $_POST['IdTaller'], $_POST['IdUnidad']);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('IdTaller', $IdTaller);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                if (isset($_POST) AND isset($_POST['NombreTema']) == true) {
                    $this->Modelo->EliminarTemaUnidad($IdUnidad);
                    $DatosPost = $_POST['NombreTema'];
                    foreach ($DatosPost as $Tema) {
                        $this->Modelo->GuardaTemas(array('IdUnidad' => $IdUnidad, 'NombreTema' => $Tema));
                    }
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Exito.html')));
                    unset($DatosPost, $IdTaller, $IdUnidad, $Plantilla);
                    exit();
                }else{
                    if($this->Modelo->EliminarTemaUnidad($IdUnidad)==true){
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Exito.html')));
                        unset($DatosPost, $IdTaller, $IdUnidad, $Plantilla);
                        exit();
                    }else {
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'AgregarUnidad', 'Error.html')));
                        unset($DatosPost, $IdTaller, $Plantilla);
                        exit();
                    }
                }
            }
            }
        }



    /**
     * Metodo Publico
     * ListadoManipularContenido()
     *
     * Volvemos a listar pero esta vez para ejercer accion
     */
    public function ListadoManipularContenido(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND $_POST['IdTaller'] != '') {
                $IdTaller = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $IdInformacionInstructor = $this->Informacion['Informacion']['IdInformacion'];
                $ConsultaTaller = $this->Modelo->ConsultarTalleres(array('tbl_talleres.IdTaller' => $IdTaller), $IdInformacionInstructor);
                $CosultaUnidades =  $this->Modelo->ConsultarUnidades($IdTaller);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Taller', $ConsultaTaller[0]);
                $Plantilla->Parametro('Unidades', $CosultaUnidades);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'ListadoUnidades', 'ListadoUnidades.html')));
                unset($Consulta, $Plantilla, $IdTaller, $CosultaUnidades, $ConsultaTaller, $IdInformacionInstructor);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * EliminaUnidad()
     *
     * Elimina la Unidad
     */
    public function EliminaUnidad(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdUnidad']) == true AND $_POST['IdUnidad'] != '') {
                $IdUnidad = NeuralCriptografia::DeCodificar($_POST['IdUnidad'], APP);
                $this->Modelo->EliminarUnidad($IdUnidad);
                $this->Modelo->EliminarTemaUnidad($IdUnidad);
            }
        }
    }

    public function ListadoAsistentesTaller(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdTaller']) == true AND !empty($_POST['IdTaller'])) {
                $IdUnidad = NeuralCriptografia::DeCodificar($_POST['IdTaller'], APP);
                $Consulta = $this->Modelo->ConsultarAsistentes($IdUnidad, $this->Informacion['Informacion']['IdInformacion']);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MisTalleres', 'Listado', 'ListadoAsistentes.html')));
                unset($IdUnidad, $Consulta, $Plantilla);
            }
        }
    }
}