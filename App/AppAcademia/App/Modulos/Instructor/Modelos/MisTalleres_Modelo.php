<?php

/**
 * Clase: MisTalleres_Modelo
 */
class MisTalleres_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }


    /**
     * Metodo Publico
     * ConsultarTalleres($Condiciones = false)
     *
     * Lista los talleres del instructor
     *
     */
    public function ConsultarTalleres($Condiciones = false, $IdInformacionInstructor = false){
        $Campos = implode(',', self::ListarColumnas('tbl_talleres', false, false, APP));
        $Campos .= ', ' . implode(',', self::ListarColumnas('tbl_periodos', false, array('Nombre' => 'Periodo'), APP));
        $Campos .= ', tbl_instructores_talleres.InstructorPrincipal';
        $SQL = "SELECT $Campos FROM tbl_talleres" .
            " INNER JOIN tbl_periodos ON tbl_talleres.IdPeriodo = tbl_periodos.IdPeriodo" .
            " INNER JOIN tbl_instructores_talleres ON tbl_talleres.IdTaller = tbl_instructores_talleres.IdTaller" .
            " WHERE tbl_talleres.Status != 'ELIMINADO' AND  tbl_instructores_talleres.IdInformacionInstructor = $IdInformacionInstructor";
        if ($Condiciones == true AND is_array($Condiciones))
            $SQL .= ' AND ' . self::ObtenerCondicionesAND($Condiciones);

        $SQL .= " ORDER BY tbl_talleres.FechaHoraCreacion DESC";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarUnidades($Condiciones = false)
     *
     * Lista las unidades del taller seleccionado
     *
     */
    public function ConsultarUnidades($Condicion = false){
        $Campos = "IdUnidad, NombreUnidad, tbl_unidades_taller.Status, tbl_talleres.IdTaller";
        $SQL = "SELECT $Campos FROM tbl_unidades_taller" .
            " INNER JOIN tbl_talleres on tbl_unidades_taller.IdTaller=tbl_talleres.IdTaller";
        $SQL .= " WHERE tbl_unidades_taller.Status='ACTIVO' AND tbl_unidades_taller.IdTaller= $Condicion";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    /**
     * Metodo Publico
     * ConsultarTemas($Condiciones = false)
     *
     * Lista los temas
     *
     */
    public function ConsultarTemas($Condicion = false){
        $Campos = "IdTema, NombreTema,tbl_temas_unidad.Status, tbl_unidades_taller.IdUnidad";
        $SQL = "SELECT $Campos FROM tbl_temas_unidad" .
            " INNER JOIN  tbl_unidades_taller on tbl_temas_unidad.IdUnidad=tbl_unidades_taller.IdUnidad";
        $SQL .= " WHERE tbl_temas_unidad.Status='ACTIVO' AND tbl_unidades_taller.IdTaller= $Condicion";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarTemasUnidades($Condiciones = false)
     *
     * Lista los temas
     *
     */
    public function ConsultarTemasUnidades($Condicion = false){
        $Campos = "IdTema, NombreTema,tbl_temas_unidad.Status, tbl_unidades_taller.IdUnidad, tbl_unidades_taller.NombreUnidad  ";
        $SQL = "SELECT $Campos FROM tbl_temas_unidad" .
            " INNER JOIN  tbl_unidades_taller on tbl_temas_unidad.IdUnidad=tbl_unidades_taller.IdUnidad";
        $SQL .= " WHERE tbl_temas_unidad.Status='ACTIVO' AND tbl_unidades_taller.IdUnidad= $Condicion";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarUnidad($Condiciones = false)
     *
     * Muestra los datos de la Unidad
     *
     */
    public function ConsultarUnidad($Condicion = false){
        $Campos = "IdUnidad, NombreUnidad";
        $SQL = "SELECT $Campos FROM tbl_unidades_taller";
        $SQL .= " WHERE tbl_unidades_taller.IdUnidad= $Condicion";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     *  ConsultarDiasHorarios($Condicion = false)
     *
     * Consulta los dias y el horario correspondiente de un taller
     * @param bool $Condicion
     * @return mixed
     */
    public function ConsultarDiasHorarios($Condicion = false){
        $Campos = "NombreDia, HoraInicio, HoraFin, tbl_taller_horarios.IdDia, tbl_taller_horarios.IdHorario";
        $SQL = "SELECT $Campos FROM tbl_taller_horarios" .
            " INNER JOIN tbl_dias ON tbl_taller_horarios.IdDia = tbl_dias.IdDia" .
            " INNER JOIN tbl_horarios ON tbl_taller_horarios.IdHorario = tbl_horarios.IdHorario";
        $SQL .= " WHERE tbl_taller_horarios.IdTaller = $Condicion";
        $SQL .= " ORDER BY tbl_dias.IdDia";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ConsultarAsistentes($Condicion = false, $IdInstructor = false){
        if($Condicion == true and $IdInstructor == true){
            $Campos = "tbl_informacion_usuarios.Correo, Nombres, ApellidoPaterno, ApellidoMaterno, Fecha_Validacion";
            $SQL = "SELECT $Campos FROM tbl_talleres_asistentes" .
                " INNER JOIN tbl_informacion_usuarios ON tbl_talleres_asistentes.IdInformacionAsistente = tbl_informacion_usuarios.IdInformacion" .
                " INNER JOIN tbl_sistema_usuarios ON tbl_informacion_usuarios.IdUsuario = tbl_sistema_usuarios.IdUsuario" .
                " LEFT JOIN tbl_activacion_cuentas ON tbl_sistema_usuarios.IdUsuario = tbl_activacion_cuentas.IdUsuario".
                " INNER JOIN tbl_instructores_talleres ON tbl_talleres_asistentes.IdTaller = tbl_instructores_talleres.IdTaller".
                " WHERE tbl_talleres_asistentes.IdTaller = $Condicion AND tbl_instructores_talleres.IdInformacionInstructor".
                " GROUP BY IdInformacionAsistente ORDER BY Fecha_Validacion DESC";
            $Consulta = $this->Conexion->prepare($SQL);
            $Consulta->execute();
            return $Consulta->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    /**
     * Metodo Publico
     * GuardaUnidad($Datos = false)
     *
     * Guarda la Unidad
     * @param bool $Datos
     * @return mixed
     */
    public function GuardaUnidad($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_unidades_taller', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardaTemas($Datos = false)
     *
     * Guarda el tema
     * @param bool $Datos
     * @return mixed
     */
    public function GuardaTemas($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            $this->Conexion->insert("tbl_temas_unidad",$Datos);
            return $this->Conexion->lastInsertId();
        }
    }

    /**
     * Metodo Publico
     *  EditaUnidad($IdUnidad = false)
     *
     * Actualizamos la Unidad
     * para que no se muestre
     * @param bool $IdUnidad
     */
    public function EditaUnidad($Datos = false, $IdUnidad = false){
        if($IdUnidad == true AND $IdUnidad != '' AND $Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->update('tbl_unidades_taller', $Datos, array('IdUnidad'=>$IdUnidad));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }
    

    /**
     * Metodo Publico
     * EliminarUnidad($IdUnidad = false)
     *
     * Actualizamos el estado de la Unidad a ELIMINADO
     * para que no se muestre
     * @param bool $IdUnidad
     */
    public function EliminarUnidad($IdUnidad = false){
        if($IdUnidad == true AND $IdUnidad != ''){
            try{
                $this->Conexion->update('tbl_unidades_taller',array('Status'=>"ELIMINADO"), array('IdUnidad'=>$IdUnidad));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * Metodo Publico
     * EliminarTemaUnidad($IdUnidad = false)
     *
     * Actualizamos el estado de la Unidad a ELIMINADO
     * para que no se muestre
     * @param bool $IdPeriodo
     */
    public function EliminarTemaUnidad($IdUnidad = false){
        if($IdUnidad == true AND $IdUnidad != ''){
            try{
                return $this->Conexion->update('tbl_temas_unidad',array('Status'=>"ELIMINADO"), array('IdUnidad'=>$IdUnidad));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }
    

}
