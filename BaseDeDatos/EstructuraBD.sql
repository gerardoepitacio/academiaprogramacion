-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla academia_db.tbl_activacion_cuentas
CREATE TABLE IF NOT EXISTS `tbl_activacion_cuentas` (
  `IdActivacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Correo` varchar(85) DEFAULT NULL,
  `NewPassword` varchar(55) DEFAULT NULL,
  `Fecha_Validacion` date DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdActivacion`),
  KEY `idu_idx` (`IdUsuario`),
  CONSTRAINT `idu` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_asistencias
CREATE TABLE IF NOT EXISTS `tbl_asistencias` (
  `IdAsistencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdTallerAsistente` bigint(20) NOT NULL,
  `FechaHora` datetime DEFAULT NULL,
  `Participo` bigint(3) DEFAULT '0',
  PRIMARY KEY (`IdAsistencia`),
  KEY `fk_tbl_asistencias_tbl_talleres_asistentes1_idx` (`IdTallerAsistente`),
  CONSTRAINT `fk_tbl_asistencias_tbl_talleres_asistentes1` FOREIGN KEY (`IdTallerAsistente`) REFERENCES `tbl_talleres_asistentes` (`IdTallerAsistente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_dias
CREATE TABLE IF NOT EXISTS `tbl_dias` (
  `IdDia` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreDia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdDia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_horarios
CREATE TABLE IF NOT EXISTS `tbl_horarios` (
  `IdHorario` bigint(20) NOT NULL AUTO_INCREMENT,
  `HoraInicio` time DEFAULT NULL,
  `HoraFin` time DEFAULT NULL,
  PRIMARY KEY (`IdHorario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_informacion_usuarios
CREATE TABLE IF NOT EXISTS `tbl_informacion_usuarios` (
  `IdInformacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Direccion` mediumtext,
  `Nombres` varchar(255) DEFAULT NULL,
  `ApellidoPaterno` varchar(255) DEFAULT NULL,
  `ApellidoMaterno` varchar(255) DEFAULT NULL,
  `Cargo` varchar(255) DEFAULT NULL,
  `TelefonoFijo1` varchar(15) DEFAULT NULL,
  `TelefonoMovil1` varchar(15) DEFAULT NULL,
  `Correo` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `NombreImagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IdInformacion`),
  KEY `idUsr_idx` (`IdUsuario`),
  CONSTRAINT `idUsr` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_instructores_talleres
CREATE TABLE IF NOT EXISTS `tbl_instructores_talleres` (
  `IdInformacionInstructor` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  `InstructorPrincipal` varchar(15) DEFAULT 'INACTIVO',
  PRIMARY KEY (`IdInformacionInstructor`,`IdTaller`),
  KEY `fk_tbl_instructores_talleres_tbl_informacion_usuarios1_idx` (`IdInformacionInstructor`),
  KEY `fk_tbl_instructores_talleres_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_instructores_talleres_tbl_informacion_usuarios1` FOREIGN KEY (`IdInformacionInstructor`) REFERENCES `tbl_informacion_usuarios` (`IdInformacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_instructores_talleres_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_periodos
CREATE TABLE IF NOT EXISTS `tbl_periodos` (
  `IdPeriodo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `FechaInicio` date DEFAULT NULL,
  `FechaFin` date DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdPeriodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPerfil` bigint(20) NOT NULL,
  `Usuario` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `IdFacebook` varchar(45) DEFAULT NULL,
  `AccessToken` mediumtext,
  PRIMARY KEY (`IdUsuario`),
  KEY `idp_idx` (`IdPerfil`),
  CONSTRAINT `idp` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios_perfil
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Central` varchar(15) DEFAULT NULL,
  `Error` varchar(15) DEFAULT NULL,
  `Administrador` varchar(15) DEFAULT NULL,
  `Instructor` varchar(15) DEFAULT NULL,
  `Asistente` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_talleres
CREATE TABLE IF NOT EXISTS `tbl_talleres` (
  `IdTaller` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPeriodo` bigint(20) NOT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Descripcion` mediumtext,
  `Lugar` varchar(255) DEFAULT NULL,
  `FechaHoraCreacion` datetime DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `NombreIcono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdTaller`),
  KEY `fk_tbl_talleres_tbl_periodo1_idx` (`IdPeriodo`),
  CONSTRAINT `fk_tbl_talleres_tbl_periodo1` FOREIGN KEY (`IdPeriodo`) REFERENCES `tbl_periodos` (`IdPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_talleres_asistentes
CREATE TABLE IF NOT EXISTS `tbl_talleres_asistentes` (
  `IdTallerAsistente` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdInformacionAsistente` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  `CalificacionAsistente` float DEFAULT NULL,
  `EvalucacionTallerista` float DEFAULT NULL,
  PRIMARY KEY (`IdTallerAsistente`),
  KEY `fk_tbl_detalle_talleres_tbl_informacion_usuarios1_idx` (`IdInformacionAsistente`),
  KEY `fk_tbl_detalle_talleres_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_detalle_talleres_tbl_informacion_usuarios1` FOREIGN KEY (`IdInformacionAsistente`) REFERENCES `tbl_informacion_usuarios` (`IdInformacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_detalle_talleres_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_taller_horarios
CREATE TABLE IF NOT EXISTS `tbl_taller_horarios` (
  `IdTallerHorarios` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdHorario` bigint(20) NOT NULL,
  `IdDia` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  PRIMARY KEY (`IdTallerHorarios`),
  KEY `fk_tbl_taller_horarios_tbl_horarios1_idx` (`IdHorario`),
  KEY `fk_tbl_taller_horarios_tbl_dias1_idx` (`IdDia`),
  KEY `fk_tbl_taller_horarios_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_taller_horarios_tbl_dias1` FOREIGN KEY (`IdDia`) REFERENCES `tbl_dias` (`IdDia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_taller_horarios_tbl_horarios1` FOREIGN KEY (`IdHorario`) REFERENCES `tbl_horarios` (`IdHorario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_taller_horarios_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_temas_unidad
CREATE TABLE IF NOT EXISTS `tbl_temas_unidad` (
  `IdTema` bigint(20) NOT NULL,
  `IdUnidad` bigint(20) NOT NULL,
  `NombreTema` mediumtext,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdTema`),
  KEY `fk_tbl_temas_unidades_tbl_temario_taller1_idx` (`IdUnidad`),
  CONSTRAINT `fk_tbl_temas_unidades_tbl_temario_taller1` FOREIGN KEY (`IdUnidad`) REFERENCES `tbl_unidades_taller` (`IdUnidad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla academia_db.tbl_unidades_taller
CREATE TABLE IF NOT EXISTS `tbl_unidades_taller` (
  `IdUnidad` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  `NombreUnidad` mediumtext,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdUnidad`),
  KEY `fk_tbl_temario_taller_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_temario_taller_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
