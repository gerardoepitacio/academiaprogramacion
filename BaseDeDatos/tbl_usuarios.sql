-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla academia_db.tbl_informacion_usuarios
CREATE TABLE IF NOT EXISTS `tbl_informacion_usuarios` (
  `IdInformacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Direccion` mediumtext,
  `Nombres` varchar(255) DEFAULT NULL,
  `ApellidoPaterno` varchar(255) DEFAULT NULL,
  `ApellidoMaterno` varchar(255) DEFAULT NULL,
  `Cargo` varchar(255) DEFAULT NULL,
  `TelefonoFijo1` varchar(15) DEFAULT NULL,
  `TelefonoMovil1` varchar(15) DEFAULT NULL,
  `Correo` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `NombreImagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IdInformacion`),
  KEY `idUsr_idx` (`IdUsuario`),
  CONSTRAINT `idUsr` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_informacion_usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_informacion_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_informacion_usuarios` (`IdInformacion`, `IdUsuario`, `Direccion`, `Nombres`, `ApellidoPaterno`, `ApellidoMaterno`, `Cargo`, `TelefonoFijo1`, `TelefonoMovil1`, `Correo`, `Status`, `NombreImagen`) VALUES
	(1, 1, NULL, 'Gerardo', 'Epitacio', 'Galvez', 'Administrador', NULL, '7471007108', 'ger.epitacio@gmail.com', 'ACTIVO', NULL),
	(2, 2, 'Asdf', 'Dexter', 'Morgan', 'Sadf', 'Cargo', '2342342342', '7471000000', 'Dexter@adp.com', 'ACTIVO', NULL),
	(3, 3, NULL, 'Instructor2', 'Apellido', NULL, NULL, NULL, '7471000000', 'Instructor2@adp.com', 'ACTIVO', NULL),
	(4, 4, 'Direccion Instructor3', 'Instructor3', 'Instructor3', 'Instructor3', 'Cargo Instructor3', '7471001001', '7471001001', 'Instructor3@adp.com', 'ACTIVO', NULL),
	(5, 5, 'Direccion Asistente', 'Asistente', 'Asistente', 'Asistente', 'Asistente', NULL, NULL, NULL, 'ACTIVO', NULL);
/*!40000 ALTER TABLE `tbl_informacion_usuarios` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPerfil` bigint(20) NOT NULL,
  `Usuario` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `IdFacebook` varchar(45) DEFAULT NULL,
  `AccessToken` mediumtext,
  PRIMARY KEY (`IdUsuario`),
  KEY `idp_idx` (`IdPerfil`),
  CONSTRAINT `idp` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_sistema_usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_sistema_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios` (`IdUsuario`, `IdPerfil`, `Usuario`, `Password`, `Status`, `IdFacebook`, `AccessToken`) VALUES
	(1, 1, 'Admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', '1152637111466204', 'EAARyFAhVOjoBADMPUZAfCAoHAkGX5THPk3CRDZAHRxvyzlzh4Lbm2Ey4UXKL8B0k3EZC8bNYGI5LosmyCG1ZCfZCZCc8WBNZANK5ZAgAxZA3L6YZAXH22pT8zo6SCDTSpOhrg6KDT1uhU2QpbJktDUtPM3jyPVYa51ep0ZD'),
	(2, 2, 'Instructor', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL),
	(3, 2, 'Instructor2', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL),
	(4, 2, 'Instructor3', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL),
	(5, 3, 'Asistente', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL);
/*!40000 ALTER TABLE `tbl_sistema_usuarios` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios_perfil
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Central` varchar(15) DEFAULT NULL,
  `Error` varchar(15) DEFAULT NULL,
  `Administrador` varchar(15) DEFAULT NULL,
  `Instructor` varchar(15) DEFAULT NULL,
  `Asistente` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_sistema_usuarios_perfil: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios_perfil` (`IdPerfil`, `Nombre`, `Status`, `Central`, `Error`, `Administrador`, `Instructor`, `Asistente`) VALUES
	(1, 'Administrador', 'ACTIVO', 'true', 'true', 'true', 'true', 'true'),
	(2, 'Instructor', 'ACTIVO', 'true', 'true', 'false', 'true', 'false'),
	(3, 'Asistente', 'ACTIVO', 'true', 'true', 'false', 'false', 'true');
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
