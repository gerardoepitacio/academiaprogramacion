-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para academia_db
CREATE DATABASE IF NOT EXISTS `academia_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `academia_db`;


-- Volcando estructura para tabla academia_db.tbl_activacion_cuentas
CREATE TABLE IF NOT EXISTS `tbl_activacion_cuentas` (
  `IdActivacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Correo` varchar(85) DEFAULT NULL,
  `NewPassword` varchar(55) DEFAULT NULL,
  `Fecha_Validacion` date DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdActivacion`),
  KEY `idu_idx` (`IdUsuario`),
  CONSTRAINT `idu` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_activacion_cuentas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_activacion_cuentas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_activacion_cuentas` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_asistencias
CREATE TABLE IF NOT EXISTS `tbl_asistencias` (
  `IdAsistencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdTallerAsistente` bigint(20) NOT NULL,
  `FechaHora` datetime DEFAULT NULL,
  `Participo` bigint(3) DEFAULT '0',
  PRIMARY KEY (`IdAsistencia`),
  KEY `fk_tbl_asistencias_tbl_talleres_asistentes1_idx` (`IdTallerAsistente`),
  CONSTRAINT `fk_tbl_asistencias_tbl_talleres_asistentes1` FOREIGN KEY (`IdTallerAsistente`) REFERENCES `tbl_talleres_asistentes` (`IdTallerAsistente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_asistencias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_asistencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_asistencias` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_dias
CREATE TABLE IF NOT EXISTS `tbl_dias` (
  `IdDia` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreDia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdDia`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_dias: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_dias` DISABLE KEYS */;
INSERT INTO `tbl_dias` (`IdDia`, `NombreDia`) VALUES
	(1, 'Lunes'),
	(2, 'Martes'),
	(3, 'Miercoles'),
	(4, 'Jueves'),
	(5, 'Viernes'),
	(6, 'Sabado'),
	(7, 'Domingo');
/*!40000 ALTER TABLE `tbl_dias` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_horarios
CREATE TABLE IF NOT EXISTS `tbl_horarios` (
  `IdHorario` bigint(20) NOT NULL AUTO_INCREMENT,
  `HoraInicio` time DEFAULT NULL,
  `HoraFin` time DEFAULT NULL,
  PRIMARY KEY (`IdHorario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_horarios: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_horarios` DISABLE KEYS */;
INSERT INTO `tbl_horarios` (`IdHorario`, `HoraInicio`, `HoraFin`) VALUES
	(1, '00:08:00', '00:09:00'),
	(2, '00:10:00', '00:12:00'),
	(3, '00:12:00', '00:14:00');
/*!40000 ALTER TABLE `tbl_horarios` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_informacion_usuarios
CREATE TABLE IF NOT EXISTS `tbl_informacion_usuarios` (
  `IdInformacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Direccion` mediumtext,
  `Nombres` varchar(255) DEFAULT NULL,
  `ApellidoPaterno` varchar(255) DEFAULT NULL,
  `ApellidoMaterno` varchar(255) DEFAULT NULL,
  `Cargo` varchar(255) DEFAULT NULL,
  `TelefonoFijo1` varchar(15) DEFAULT NULL,
  `TelefonoMovil1` varchar(15) DEFAULT NULL,
  `Correo` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `NombreImagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IdInformacion`),
  KEY `idUsr_idx` (`IdUsuario`),
  CONSTRAINT `idUsr` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_informacion_usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_informacion_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_informacion_usuarios` (`IdInformacion`, `IdUsuario`, `Direccion`, `Nombres`, `ApellidoPaterno`, `ApellidoMaterno`, `Cargo`, `TelefonoFijo1`, `TelefonoMovil1`, `Correo`, `Status`, `NombreImagen`) VALUES
	(1, 1, NULL, 'Gerardo', 'Epitacio', 'Galvez', 'Administrador', NULL, '7471007108', 'ger.epitacio@gmail.com', 'ACTIVO', NULL),
	(2, 2, 'Asdf', 'Dexter', 'Morgan', 'Sadf', 'Cargo', '2342342342', '7471000000', 'Dexter@adp.com', 'ACTIVO', NULL),
	(3, 3, NULL, 'Instructor2', 'Apellido', NULL, NULL, NULL, '7471000000', 'Instructor2@adp.com', 'ACTIVO', NULL),
	(4, 4, 'Direccion Instructor3', 'Instructor3', 'Instructor3', 'Instructor3', 'Cargo Instructor3', '7471001001', '7471001001', 'Instructor3@adp.com', 'ACTIVO', NULL),
	(5, 5, 'Direccion Asistente', 'Asistente', 'Asistente', 'Asistente', 'Asistente', NULL, NULL, NULL, 'ACTIVO', NULL);
/*!40000 ALTER TABLE `tbl_informacion_usuarios` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_instructores_talleres
CREATE TABLE IF NOT EXISTS `tbl_instructores_talleres` (
  `IdInformacionInstructor` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  `InstructorPrincipal` varchar(15) DEFAULT 'INACTIVO',
  PRIMARY KEY (`IdInformacionInstructor`,`IdTaller`),
  KEY `fk_tbl_instructores_talleres_tbl_informacion_usuarios1_idx` (`IdInformacionInstructor`),
  KEY `fk_tbl_instructores_talleres_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_instructores_talleres_tbl_informacion_usuarios1` FOREIGN KEY (`IdInformacionInstructor`) REFERENCES `tbl_informacion_usuarios` (`IdInformacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_instructores_talleres_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_instructores_talleres: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_instructores_talleres` DISABLE KEYS */;
INSERT INTO `tbl_instructores_talleres` (`IdInformacionInstructor`, `IdTaller`, `InstructorPrincipal`) VALUES
	(1, 22, 'ACTIVO'),
	(1, 23, 'INACTIVO'),
	(1, 24, 'INACTIVO'),
	(1, 25, 'INACTIVO'),
	(2, 19, 'ACTIVO'),
	(2, 20, 'INACTIVO'),
	(2, 21, 'ACTIVO'),
	(2, 22, 'INACTIVO'),
	(2, 23, 'ACTIVO'),
	(2, 24, 'ACTIVO'),
	(2, 25, 'ACTIVO'),
	(3, 19, 'INACTIVO'),
	(3, 20, 'ACTIVO'),
	(3, 22, 'INACTIVO');
/*!40000 ALTER TABLE `tbl_instructores_talleres` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_periodos
CREATE TABLE IF NOT EXISTS `tbl_periodos` (
  `IdPeriodo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `FechaInicio` date DEFAULT NULL,
  `FechaFin` date DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdPeriodo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_periodos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_periodos` DISABLE KEYS */;
INSERT INTO `tbl_periodos` (`IdPeriodo`, `Nombre`, `FechaInicio`, `FechaFin`, `Status`) VALUES
	(1, 'Septiembre2016-Diciembre2016', '2016-07-25', '2016-07-25', 'ACTIVO'),
	(2, 'Enero2017-Julio2017', '2017-01-07', '2017-07-07', 'ACTIVO'),
	(3, 'DFG', '2016-09-05', '2016-09-29', 'ACTIVO');
/*!40000 ALTER TABLE `tbl_periodos` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_prueba
CREATE TABLE IF NOT EXISTS `tbl_prueba` (
  `idtbl_prueba` int(11) NOT NULL,
  `tbl_taller_horarios_IdTallerHorarios` bigint(20) NOT NULL,
  PRIMARY KEY (`idtbl_prueba`),
  KEY `fk_tbl_prueba_tbl_taller_horarios1_idx` (`tbl_taller_horarios_IdTallerHorarios`),
  CONSTRAINT `fk_tbl_prueba_tbl_taller_horarios1` FOREIGN KEY (`tbl_taller_horarios_IdTallerHorarios`) REFERENCES `tbl_taller_horarios` (`IdTallerHorarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_prueba: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_prueba` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_prueba` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPerfil` bigint(20) NOT NULL,
  `Usuario` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  `IdFacebook` varchar(45) DEFAULT NULL,
  `AccessToken` mediumtext,
  PRIMARY KEY (`IdUsuario`),
  KEY `idp_idx` (`IdPerfil`),
  CONSTRAINT `idp` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_sistema_usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_sistema_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios` (`IdUsuario`, `IdPerfil`, `Usuario`, `Password`, `Status`, `IdFacebook`, `AccessToken`) VALUES
	(1, 1, 'Admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', '', ''),
	(2, 2, 'Instructor', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL),
	(3, 2, 'Instructor2', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL),
	(4, 2, 'Instructor3', '7108d459620074b4bc7e3040b43981a7512ce12d23da12ba45ca30db3eaa669b', 'ACTIVO', NULL, NULL),
	(5, 3, 'Asistente', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO', NULL, NULL);
/*!40000 ALTER TABLE `tbl_sistema_usuarios` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_sistema_usuarios_perfil
CREATE TABLE IF NOT EXISTS `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Central` varchar(15) DEFAULT NULL,
  `Error` varchar(15) DEFAULT NULL,
  `Administrador` varchar(15) DEFAULT NULL,
  `Instructor` varchar(15) DEFAULT NULL,
  `Asistente` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_sistema_usuarios_perfil: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios_perfil` (`IdPerfil`, `Nombre`, `Status`, `Central`, `Error`, `Administrador`, `Instructor`, `Asistente`) VALUES
	(1, 'Administrador', 'ACTIVO', 'true', 'true', 'true', 'true', 'true'),
	(2, 'Instructor', 'ACTIVO', 'true', 'true', 'false', 'true', 'false'),
	(3, 'Asistente', 'ACTIVO', 'true', 'true', 'false', 'false', 'true');
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_talleres
CREATE TABLE IF NOT EXISTS `tbl_talleres` (
  `IdTaller` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPeriodo` bigint(20) NOT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Descripcion` mediumtext,
  `Lugar` varchar(255) DEFAULT NULL,
  `FechaHoraCreacion` datetime DEFAULT NULL,
  `Status` varchar(15) DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdTaller`),
  KEY `fk_tbl_talleres_tbl_periodo1_idx` (`IdPeriodo`),
  CONSTRAINT `fk_tbl_talleres_tbl_periodo1` FOREIGN KEY (`IdPeriodo`) REFERENCES `tbl_periodos` (`IdPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_talleres: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_talleres` DISABLE KEYS */;
INSERT INTO `tbl_talleres` (`IdTaller`, `IdPeriodo`, `Nombre`, `Descripcion`, `Lugar`, `FechaHoraCreacion`, `Status`) VALUES
	(19, 1, 'POO orientada a objetos', 'En este taller aprenderemos los pilares basicos de la programacion orientada a objetos.', 'Laboratorio II de la Unidad Academica de Matematicas', '2016-08-09 06:12:24', 'ACTIVO'),
	(20, 1, 'C++ Avanzado', 'Temas os de programacion', 'UAI', '2016-08-18 10:34:10', 'ACTIVO'),
	(21, 2, 'sadf', 'asdf', 'asdf', '2016-08-26 04:01:31', 'ACTIVO'),
	(22, 2, 'Programacion orientada a aspectos', 'Programacion orientada a aspectos', 'UAM', '2016-09-06 18:31:42', 'ACTIVO'),
	(23, 2, 'Programacion avanzada', 'Programacion avanzada', 'Programacion avanzada', '2016-09-06 18:58:26', 'ACTIVO'),
	(24, 2, 'Programacion avanzada 2', 'Programacion avanzada 2', 'UAM', '2016-09-06 19:03:40', 'ACTIVO'),
	(25, 2, 'Programacion avanzada 3', 'Programacion avanzada 3', 'UAM', '2016-09-06 19:09:42', 'ACTIVO');
/*!40000 ALTER TABLE `tbl_talleres` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_talleres_asistentes
CREATE TABLE IF NOT EXISTS `tbl_talleres_asistentes` (
  `IdTallerAsistente` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdInformacionAsistente` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  `CalificacionAsistente` float DEFAULT NULL,
  `EvalucacionTallerista` float DEFAULT NULL,
  PRIMARY KEY (`IdTallerAsistente`),
  KEY `fk_tbl_detalle_talleres_tbl_informacion_usuarios1_idx` (`IdInformacionAsistente`),
  KEY `fk_tbl_detalle_talleres_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_detalle_talleres_tbl_informacion_usuarios1` FOREIGN KEY (`IdInformacionAsistente`) REFERENCES `tbl_informacion_usuarios` (`IdInformacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_detalle_talleres_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_talleres_asistentes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_talleres_asistentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_talleres_asistentes` ENABLE KEYS */;


-- Volcando estructura para tabla academia_db.tbl_taller_horarios
CREATE TABLE IF NOT EXISTS `tbl_taller_horarios` (
  `IdTallerHorarios` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdHorario` bigint(20) NOT NULL,
  `IdDia` bigint(20) NOT NULL,
  `IdTaller` bigint(20) NOT NULL,
  PRIMARY KEY (`IdTallerHorarios`),
  KEY `fk_tbl_taller_horarios_tbl_horarios1_idx` (`IdHorario`),
  KEY `fk_tbl_taller_horarios_tbl_dias1_idx` (`IdDia`),
  KEY `fk_tbl_taller_horarios_tbl_talleres1_idx` (`IdTaller`),
  CONSTRAINT `fk_tbl_taller_horarios_tbl_dias1` FOREIGN KEY (`IdDia`) REFERENCES `tbl_dias` (`IdDia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_taller_horarios_tbl_horarios1` FOREIGN KEY (`IdHorario`) REFERENCES `tbl_horarios` (`IdHorario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_taller_horarios_tbl_talleres1` FOREIGN KEY (`IdTaller`) REFERENCES `tbl_talleres` (`IdTaller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla academia_db.tbl_taller_horarios: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_taller_horarios` DISABLE KEYS */;
INSERT INTO `tbl_taller_horarios` (`IdTallerHorarios`, `IdHorario`, `IdDia`, `IdTaller`) VALUES
	(1, 1, 1, 21),
	(2, 1, 1, 21),
	(3, 1, 1, 21),
	(4, 1, 1, 23),
	(5, 1, 2, 23),
	(6, 1, 1, 23),
	(7, 1, 1, 23),
	(8, 1, 1, 23),
	(9, 1, 1, 24),
	(10, 2, 2, 24),
	(11, 3, 3, 24),
	(12, 1, 3, 24),
	(13, 2, 5, 24),
	(14, 1, 1, 25),
	(15, 2, 2, 25),
	(16, 3, 3, 25),
	(17, 1, 4, 25),
	(18, 2, 5, 25);
/*!40000 ALTER TABLE `tbl_taller_horarios` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
